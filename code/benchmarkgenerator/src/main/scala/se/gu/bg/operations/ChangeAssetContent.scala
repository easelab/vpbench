package se.gu.bg.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.Asset
import se.gu.vp.operations.ChangeAsset

case class ChangeAssetContent ( assetToChange:Asset, newContent:List[String]) extends ChangeAsset with Logging{
  override def perform(): Boolean = {
    ChangeAsset(assetToChange)
    assetToChange.content = Some(newContent)
    true
  }
}
object ChangeAssetContent{
  def apply(assetToChange: Asset, newContent: List[String]): ChangeAssetContent = new ChangeAssetContent(assetToChange, newContent)
}

