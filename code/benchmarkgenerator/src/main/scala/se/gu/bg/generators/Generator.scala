package se.gu.bg.generators

import se.gu.vp.operations.Operation

trait Generator {
  def generate: Option[Operation]
}