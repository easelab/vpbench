package se.gu.bg.generators

import se.gu.vp.model._
import se.gu.vp.operations.CloneAsset
import se.gu.vp.operations.Utilities._

import scala.util.Random

class CloneAssetGenerator(val rootAsset:Asset) extends Generator {
  def generateCandidates: Set[Asset] = {
    Nil.toSet
  }

  override def generate: Option[CloneAsset] = {
    val assets = transformASTToList(rootAsset)

    // filter root as that is the only asset, that cannot be cloned
    // filter blocks out for now, as the possibility of calling CloneAsset on blocks needs to be discussed more
    val filteredAssets = assets.filter(a => a.assetType != VPRootType && a.assetType != BlockType)
    val original = filteredAssets(Random.nextInt(filteredAssets.length))
    val originalType = original.assetType

    // current implementation allows for cloning an asset beneath the same exact parent
    val (genTargetContainer,genInsertAfter) = generateTargetContainer(assets, originalType)

    // for debugging
    println(s"Clone $original into $genTargetContainer")

    val generated = new CloneAsset(source = original, targetContainer = genTargetContainer, insertAfter = genInsertAfter)
    Some(generated)
  }

  def generateTargetContainer(assets: List[Asset], originalType: AssetType) : Tuple2[Asset, Option[Asset]] = {
    // fpr which types do we need to filter?
    // calculated based on AssetType of source, a clone can only be contained by assets with a certain AssetType
    val filterTypes : Set[AssetType] = originalType match {
      case RepositoryType => Set(VPRootType)
      case FolderType => Set(RepositoryType, FolderType)
      case FileType => Set(RepositoryType, FolderType)
      case MethodType => Set(FileType)  //language-dependent, might be MethodType, too. e.g. in scala
      case BlockType => Set(MethodType, FileType)
    }

    // filter targetContainers based on their possible types and select one of them
    val possibleTargets = assets.filter(a => filterTypes.contains(a.assetType))
    val selectedTarget = possibleTargets(Random.nextInt(possibleTargets.length))

    // if the source is cloned into a file, generate an asset, after which it should be placed
    // MethodType is in theory only required in case youre cloning Blocks and/or nesting Methods, does not interfere with the logic though
    if ((selectedTarget.assetType == FileType || selectedTarget.assetType == MethodType) && selectedTarget.children.length != 0)
    {
      val children = selectedTarget.children
      val insertAfter = children(Random.nextInt(children.length))
      (selectedTarget, Some(insertAfter))
    }
    else
    {
      (selectedTarget,None)
    }
  }
}

object CloneAssetGenerator{
  def apply(rootAsset: Asset):CloneAssetGenerator = new CloneAssetGenerator(rootAsset)
}