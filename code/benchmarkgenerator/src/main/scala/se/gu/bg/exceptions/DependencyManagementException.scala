package se.gu.bg.exceptions

case class DependencyManagementException(message: String) extends Exception