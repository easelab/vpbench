package se.gu.bg

import java.io.{File, FileWriter}
import java.text.SimpleDateFormat
import java.util.Calendar
import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}

import se.gu.bg.RunEvaluation.postprocessJavaParsing
import se.gu.bg.TestConfigurationProvider.{createAlgorithmsProj, createDisruptorProj, createEventbusProj, createJadxProj, createJsonJavaProj, createNLPProj, createReactiveXProj, createStructurizrProj, createTeammatesProj}
import se.gu.bg.generators.{AddAssetGenerator, AddFeatureGenerator, AddLineGenerator, ChangeAssetGenerator, CloneAssetGenerator, CloneFeatureGenerator, DeleteLineGenerator, Generator, PropagateFeatureGenerator, RemoveFeatureGenerator, ReplaceLineGenerator, ReproduceCloneFeatureGenerator2}
import se.gu.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.vp.model.{Asset, FileType, FolderType}
import se.gu.vp.operations.CalculatorSimulation.ASTModificationHelpers.handleFeatureModel
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.getListOfSubDirectories
import se.gu.vp.operations.CalculatorSimulation._
import se.gu.vp.operations.JavaParser.JavaParserMain.{parseJavaFile, readInJavaAssetTree}
import se.gu.vp.operations.Replay.AssetHelpers
import se.gu.vp.operations.Utilities.{getassetbyname, transformASTToList}
import se.gu.vp.operations.{AddAsset, CloneFeature, SerialiseAssetTree}

import scala.::
import scala.io.Source

object CalculatorTest {

  def main(args: Array[String]) : Unit =
  {
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm")
    val datetime = Calendar.getInstance().getTime()
    val targetpath =  "D:\\Dokumente\\_Studium\\masterarbeit\\test"
    val targetdatepath = targetpath + "\\" + dateFormat.format(datetime)
    val targetdatedir = new File(targetdatepath)
    targetdatedir.mkdir()

    //val readInPath = "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\vp4\\vp\\CalculatorRoot"
    //val readInPath = "D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\JSON-javaRoot"
    val readInPath = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot"
    //val readInPath = "D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\RxJavaRoot"
    val ast = readInJavaAssetTree(readInPath)
    ast match {
      case None => return
      case Some(astroot) =>

        SerialiseAssetTree(astroot, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir")
//        val compiled1 = executeGradle("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir\\CalculatorRoot\\BasicCalculator", "compileJava")
//        println(compiled1)

        val structurizrProject = createStructurizrProj()
        val reactiveXProject = createReactiveXProj()
        val jadxProject = createJadxProj()
        val disruptorProject = createDisruptorProj()
        val jsonJavaProject = createJsonJavaProj()
        val algorithmsProject = createAlgorithmsProj()
        val eventBusProject = createEventbusProj()
        val coreNLPProject = createNLPProj()
        val teammatesProject = createTeammatesProj()
        //val rootProjects = List(structurizrProject, reactiveXProject, jadxProject, disruptorProject)
        //val rootProjects = List(disruptorProject, jsonJavaProject)
        //val rootProjects = List(reactiveXProject, jadxProject, disruptorProject)
        val rootProjects = List(disruptorProject)
        //val rootProjects = List(disruptorProject)
//        val rootProjects = List(teammatesProject)
        //val rootProjects = List(reactiveXProject)

//        val addAssetGen = AddAssetGenerator(astroot)
        val delLineGen = DeleteLineGenerator(astroot, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath, 0.8)
        val addLineGen = AddLineGenerator(astroot, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath, 0.8)
        val repLineGen = ReplaceLineGenerator(astroot, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath, 0.8)
//        val cloneAssetGen = CloneAssetGenerator(astroot)

        val filterInsertionPoints = List(
          getassetbyname("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\CalculatorRepo\\src\\main\\java", astroot).get
//          getassetbyname("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\JSON-javaRoot\\JSON-java\\src\\main\\java", astroot).get
//          getassetbyname("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\RxJavaRoot\\RxJava\\src\\main\\java", astroot).get
        )
        postprocessJavaParsing(astroot, filterInsertionPoints)

        val addFeatureGen = AddFeatureGenerator(astroot, rootProjects, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath, Some(filterInsertionPoints))

        val remFeatureGen = RemoveFeatureGenerator(astroot, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath)
        //val generators = List(delLineGen, addLineGen, repLineGen)
        //val generators = List(delLineGen)
        //val generators = List(addFeatureGen)
        val generators = List(addFeatureGen, remFeatureGen)
        //        val generators = List(addFeatureGen, delLineGen, addLineGen, repLineGen, remFeatureGen)
//        val generators = List(addAssetGen, changeAssetGen, cloneAssetGen, addFeatureGen)
//        val config : Map[Generator,Double] = Map(repLineGen -> 0.33, addLineGen -> 0.33, delLineGen -> 0.34)
//        val config : Map[Generator,Double] = Map(addFeatureGen -> 0.33, remFeatureGen -> 0.33, repLineGen -> 0.11, addLineGen -> 0.11, delLineGen -> 0.12)
//        val opGen = new BenchmarkGenerationRunner(astroot, generators, Some(config))
        val opGen = new BenchmarkGenerationRunner(astroot, generators)//, Some(config))
        opGen.run(80, targetdatepath, None)
        val a = 1
//        for (i <- 1 to 5)
//        {
//          opGen.generateNextOperation
//          astroot.prettyprint
//
//          val currDir = new File(targetdatepath + s"\\v$i")
//          currDir.mkdir()
//          SerialiseAssetTree(astroot, currDir.getAbsolutePath)
//        }

//        val cfGen = CloneFeatureGenerator(astroot)
//        val rfcGen = ReproduceCloneFeatureGenerator2(astroot)
//        val pfGen = PropagateFeatureGenerator(astroot)
//        for (i <- 1 to 10)
//        {
//          //val gen = cfGen.generate
//          val gen = rfcGen.generate
//          gen match {
//            case None =>
//              println("Nothing ventured.")
//            case Some(CloneFeature(sf, tf)) => //log -> into commit message?
//              println(s"Clone $sf into $tf.")
//          }
//          astroot.prettyprint
//
//          val currDir = new File(targetdatepath + s"\\v$i")
//          currDir.mkdir()
//          SerialiseAssetTree(astroot, currDir.getAbsolutePath)
//        }

      //            for (i <- 4 to 10) {
      //              val gen = pfGen.generate
      //              //            gen match {
      //              //              case None =>
      //              //                println("Nothing ventured.")
      //              //              case Some(CloneFeature(sf,tf)) => //log -> into commit message?
      //              //                println(s"Clone $sf into $tf.")
      //              //            }
      //              astroot.prettyprint
      //
      //              val currDir = new File(targetdatepath + s"\\v$i")
      //              currDir.mkdir()
      //              SerialiseAssetTree(astroot, currDir.getAbsolutePath)
      //            }
    }
  }

}