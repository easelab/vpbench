package se.gu.bg.generators

import se.gu.bg.Utilities.{resolveFeatureFromPath, resolveFeatureModelFromPath}
import se.gu.vp.model._
import se.gu.vp.operations.{CloneFeature, PropagateFeature}
import se.gu.vp.operations.Utilities._

import scala.util.Random

class PropagateFeatureGenerator(val rootAsset:Asset) extends Generator {
  def generateCandidates: Set[Asset] = {
    Nil.toSet
  }

  override def generate: Option[PropagateFeature] = {
    val assets = transformASTToList(rootAsset)

    // can there be duplicates in naming between features in database in different feature models?
    val traces = FeatureTraceDatabase.traces
    // calculate all features, that are referenced in the featuretrace-db
    val involvedFeatures : Set[Tuple2[Feature,Feature]] = (for {trace <- traces} yield (trace.source,trace.target)).toSet
    val sourceFeatures = involvedFeatures.map(feat => feat._1)
    val targetFeatures = involvedFeatures.map(feat => feat._2)

    // lists all features including the assetpaths to their defining feature models
    // can be further optimized by limiting to involved here
    val features_asPath = for {
      asset <- assets
      if asset.featureModel.isDefined
      asPath = computeAssetPath(asset)
      feature <- transformFMToList(asset.featureModel.get)
    } yield (asPath,feature)

    // filters for all features, referenced by the source relation and generates entire paths
    val sourceFeaturePaths = for {
      (asPath,feature) <- features_asPath
      if sourceFeatures.contains(feature)
      fePath = computeFeaturePath(feature)
    } yield {
      Path(rootAsset, asPath ++ fePath)
    }

    // return None, if no suitable target for PropagateFeature exists
    if (sourceFeaturePaths.length == 0)
      return None

    // select sourceFeature
    val sourceFeaturePath = sourceFeaturePaths(Random.nextInt(sourceFeaturePaths.length))
    val sourceFeature = resolveFeatureFromPath(sourceFeaturePath).get

    // filters features to the ones, targeted by the selected source
    val validTargets = traces.filter(trace => trace.source == sourceFeature).map(trace => trace.target)

    // generates all targetpaths for the selected source feature
    val targetFeaturePaths = for {
      (asPath,feature) <- features_asPath
      if validTargets.contains(feature)
      fePath = computeFeaturePath(feature)
    } yield {
      Path(rootAsset, asPath ++ fePath)
    }

    // select targetfeature
    val targetFeaturePath = targetFeaturePaths(Random.nextInt(targetFeaturePaths.length))

    Some(PropagateFeature(sourceFeaturePath,targetFeaturePath))
  }
}

object PropagateFeatureGenerator{
  def apply(rootAsset: Asset):PropagateFeatureGenerator = new PropagateFeatureGenerator(rootAsset)
}