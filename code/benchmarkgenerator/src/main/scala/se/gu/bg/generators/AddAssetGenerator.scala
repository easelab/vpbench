package se.gu.bg.generators

import se.gu.vp.model._
import se.gu.vp.operations.AddAsset
import se.gu.vp.operations.Utilities._
import scala.util.Random

// completely new Assets and readd assets that were previously contained in the project, but removed at some point -> different kind of generator -> suboperation
// could make more sense to first conceptually construct an asset and choose a suitable parent afterwards? or something interconnected?

class AddAssetGenerator(val rootAsset:Asset) extends Generator {
  def generateCandidates: Set[Asset] = {
    Nil.toSet
  }

  override def generate: Option[AddAsset] = {
    val assets = transformASTToList(rootAsset)

    // filter out BlockType, as a Block cannot be the parent of another asset
    val filteredAssets = assets.filter(a => a.assetType != BlockType)
    val parent = filteredAssets(Random.nextInt(filteredAssets.length))
    val parentType = parent.assetType

    val assetInfo = generateAssetInfo(parentType)
    val newAsset = Asset(name = assetInfo._1, assetType = assetInfo._2)

    // for debugging
    println(s"Add Asset $newAsset underneath $parent")

    val generated = new AddAsset(asset = newAsset, parent = parent)
    Some(generated)
  }

  def generateAssetInfo(parentType: AssetType) : Tuple2[String, AssetType] = {
    parentType match {
      case VPRootType => ("Repo", RepositoryType)
      case RepositoryType =>
        Random.nextInt(2) match {
          case 0 => ("Folder", FolderType)
          case 1 => ("File", FileType)
        }
      case FolderType =>
        Random.nextInt(2) match {
          case 0 => ("Folder", FolderType)
          case 1 => ("File", FileType)
        }
      case FileType =>
        Random.nextInt(2) match {
          case 0 => ("Method", MethodType)
          case 1 => ("Block", BlockType)
        }
      case MethodType => ("Block", BlockType)
    }
  }
}

object AddAssetGenerator{
  def apply(rootAsset:Asset):AddAssetGenerator = new AddAssetGenerator(rootAsset)
}