package se.gu.bg.generators

import java.io.File

import se.gu.bg.model.{Dependency, ExternalDependency, Project, ProjectDependency, TestCase}
import se.gu.vp.model.{Asset, AssetType, BlockType, Feature, FileType, FolderType, MethodType, RepositoryType, TraceDatabase, VPRootType}
import se.gu.vp.operations.{AddAsset, ChangeAsset, CloneAsset, MapAssetToFeature, MoveFeature, Operation, SerialiseAssetTree}

import scala.sys.process.Process
import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}
import java.util
import java.util.Comparator

import se.gu.bg.HandleJavaDependencies.{addSubprojectsToSettings, copyGradleProjectWrapper, copyProject, getDependenciesRecursively, mapClassToJava}

import scala.collection.JavaConversions._
import se.gu.bg.Utilities.{cleanDirectory, createDictIfNotExists, findFileParentAsset, findRepoOnUpPath, isTransitiveChildOf, replaceStringSlice}
import se.gu.bg.gradlehandler.GradleHandler.{getDependenciesAndClosureBorders, javaLibraryPluginRelevantConfigs, mapProjectDepsToNewStructure, mapTestCompileToApiElements}
import se.gu.bg.gradlehandler.GradleParsingUtilities.getClosureIndices
import se.gu.bg.model.TestCase.parseLineToTestcase
import com.rits.cloning.Cloner
import org.gradle.api.tasks.TaskExecutionException
import org.gradle.tooling.GradleConnector
import org.gradle.tooling.model.{GradleProject, ProjectModel}
import org.gradle.tooling.model.build.BuildEnvironment
import org.gradle.tooling.model.gradle.GradleBuild
import org.gradle.tooling.model.idea.IdeaProject
import se.gu.bg.GradleProgressListener
import se.gu.bg.exceptions.{DependencyManagementException, TestcaseExtractionException}
import se.gu.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.bg.operations.{AddAssetAtPosition, AddExternalFeature, ChangeAssetContent}
import se.gu.vp.operations.JavaParser.JavaParserMain.parseJavaFile
import se.gu.vp.operations.Utilities.{deepclone, findAssetByName, getImmediateAncestorFeatureModel, getassetbyname, transformASTToList}

import scala.collection.mutable
import scala.util.Random

class AddFeatureGenerator(val rootAsset: Asset, val rootProjects: Seq[Project], val targetPathString: String, val readInPathString: String, val filterInsertionPoints: Option[List[Asset]] = None) extends Generator {
  val jdepsPath = file.Paths.get("C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin")
  val unitTestScribePath = file.Paths.get("D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\UnitTestScribe\\WM.UnitTestScribe\\bin\\Release\\TeaCap.exe")
  val initialisationPath = file.Paths.get("D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases")
  val srcmlPath = file.Paths.get("D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\UnitTestScribe\\SrcML")
  // TODO: needs to be integrated with the entire Generator process -> either the target path here needs to change (-> val to var, when using a new folder for each version) or introduce git services to always use this in same place OR: use this process to just test it somehow? -> needs to be integrated with VP and BMGen
  // TODO: Plan for this is written down in notebook -> use targetpath for testing it before converting every changes into VP-operations
  val targetPath = file.Paths.get(targetPathString)
  val readInPath = file.Paths.get(readInPathString)
  val ignoredPathNames = List(".gradle", "build", "featuremodel.txt")
  val blacklist: mutable.HashSet[Tuple2[Project, TestCase]] = mutable.HashSet[Tuple2[Project, TestCase]]()

  initialise

  def getTestcaseListFromProject(testSrcSetPath: String, projectPath: String): Option[Path] = {
    val testcasesDir = initialisationPath.resolve(projectPath.replace(':', '\\'))
    val testcasesFile = testcasesDir.resolve("idtestcases.txt")
    createDictIfNotExists(testcasesDir)
    try {
      // just commented to allow for faster execution, while testcase-files exist
      val result = Process(Seq(unitTestScribePath.toString,
        "fid_testcase_save",
        "--loc", testSrcSetPath,
        "--srcmlPath", srcmlPath.toString,
        "--outputLoc", testcasesFile.toString
      )).!
    } catch {
      case _ =>
    }

    if (Files.exists(testcasesFile)) {
      Some(testcasesFile)
    }
    else {
      None
    }
  }

  def initialise: Unit = {
    for {
      rootProject <- rootProjects
      project <- rootProject.listAllProjectsRecursively
      testSrcSetPath = project.getAbsolutePathSrc.resolve(project.srcSetTest)
      projectPath = project.getProjectPathIncludingRoot
    } yield {
      project.testcases = getTestcaseListFromProject(testSrcSetPath.toString, projectPath) match {
        case None => List()
        case Some(file) =>
          Files.readAllLines(file)
            .map(line => parseLineToTestcase(line))
      }
    }
    //    rootProjects.flatMap(project => project.listAllProjectsRecursively)
    //      .map(project => (project, project.getAbsolutePathSrc.resolve(project.srcSetTest), project.getProjectPathIncludingRoot))
    //      .map(project => (project._1, getTestcaseListFromProject(project._2.toString, project._3)))
    //      .map()
    //    val result = Process(Seq(unitTestScribePath.toString,
    //      "",
    //      "--loc", "",
    //      "--srcmlPath", "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\UnitTestScribe\\SrcML",
    //      "--outputLoc", "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases",
    //    )).!!
  }

  // AddFeature does not exist as operation in VP
  override def generate: Option[AddExternalFeature] = {
    val methodAssetList = if (filterInsertionPoints.isDefined) {
      transformASTToList(rootAsset)
        .filter(asset => asset.assetType == MethodType)
        .filter(method => isTransitiveChildOf(method, filterInsertionPoints.get))
    } else {
      transformASTToList(rootAsset)
        .filter(asset => asset.assetType == MethodType)
    }

    val insertionMethod = methodAssetList(Random.nextInt(methodAssetList.length))
    val insertionNumber = if (insertionMethod.children.length == 0) {
      0
    } else {
      Random.nextInt(insertionMethod.children.length)
    }
    // insertionNumber is not yet used
    val insertionPoint = (insertionMethod, insertionNumber)

    val projTestTuples = rootProjects.map(proj => proj.listAllProjectsRecursively).flatten
      .map(proj => proj.testcases.map(test => (proj, test))).flatten
      .filter(elem => !blacklist.contains(elem))
    if (projTestTuples.length == 0) {
      // TODO: react somehow, if no transplantable testcases remain
      None
    } else {
      val (subProject, testCase) = projTestTuples(Random.nextInt(projTestTuples.length))

      // TODO: read in paper, for now intuition is: if same testcase should be reused somewhere else after being successfully transplanted -> use a different operation (CloneFeature or similar)
      // could in theory still work somewhere else, if it doesnt work
      // if it does, add it somewhere else, too? -> if same feature is added to some repo, is that still AddFeature? Or rather AddAsset, CloneFeature or CloneAsset?
      blacklist.add((subProject, testCase))
      try {
        extractTestcase(insertionPoint, subProject, testCase)
      } catch {
        case ex: TestcaseExtractionException =>
          None
        case ex: DependencyManagementException =>
          val testClass = testCase.className
          subProject.testcases.filter(tc => tc.className == testClass)
            .foreach(test => blacklist.add((subProject, test)))
          None
      }
    }
  }

  def addNewDependency(buildFile: Path, dependency: Dependency): Unit = {
    val buildFileContent = Files.readString(buildFile)

    // Assumes at least an empty dependencies block in systems build.gradle
    // No support for allprojects- or subprojects-dependencies here
    val depRegEx = raw"dependencies\s*\Q{\E".r
    val dependencyMatch = depRegEx.findAllMatchIn(buildFileContent).toList.head

    val (dependencies, _, depClosureEndInd) = getDependenciesAndClosureBorders(buildFileContent, dependencyMatch.start)

    var editableDependencies = dependencies
    if (!dependencies.contains(dependency)) {
      editableDependencies = editableDependencies ++ List(dependency)
    }

    val newDependencyClosure = editableDependencies
      // create dependenciesString
      .map(dep => dep.toString)
      .mkString("\n\t")
    // put dependencies into identified block
    val dependenciesString = s"dependencies {\n\t$newDependencyClosure\n}"
    // replace current dependencies block with created one
    val newBuildFileContent = replaceStringSlice(buildFileContent, dependenciesString, dependencyMatch.start, dependencyMatch.start + depClosureEndInd)

    Files.writeString(buildFile, newBuildFileContent)
  }

  def extractFileContent(subProject: Project, testCase: TestCase): Path = {
    // Extract testcase and relevant in-file dependencies
    val loc = subProject.getAbsolutePathSrc().resolve(subProject.srcSetTest).toString
    val outputLoc = initialisationPath.resolve(subProject.getProjectPathIncludingRoot.replace(':', '\\')).resolve(testCase.toString.replace('.', '\\'))
    createDictIfNotExists(outputLoc)

    val result = Process(Seq(unitTestScribePath.toString,
      "extractTestcase",
      "--loc", loc,
      "--srcmlPath", srcmlPath.toString,
      "--outputLoc", outputLoc.toString,
      "--namespace", testCase.packageName,
      "--class", testCase.className,
      "--test", testCase.testName
    )).!

    outputLoc
  }

  def extractOutOfFileDependencies(repo: Asset, repoPath: Path, subProject: Project, testCase: TestCase): Set[Tuple2[Project,Seq[Path]]] = {
    // Get dependencies
    val rootProject = subProject.getRootProject()
    val classFile = subProject.getAbsolutePathSrc.resolve("build\\classes\\java\\test\\").resolve(testCase.getClassFilePath)

    // Get Map from jar-names to their corresponding projects, assumes unique jar-names.
    val jarPathToProjectMap = rootProject.getJarToProjMap()
    val jarNameToProjectMap = jarPathToProjectMap.map(tuple => (tuple._1.getFileName.toString, tuple._2))

    // Gets all required .class-files together with their containing .jar-files
    val elements = getDependenciesRecursively(rootProject, subProject, classFile, jdepsPath.toString, 0, Nil)

    // Maps the list of .class-files per .jar to a list of .java-files per Project
    val depsPerJar = elements.groupBy(el => el._2)
    val filesPerJar = for {
      jar <- depsPerJar.keySet
      proj = jarNameToProjectMap(jar)
    } yield {
      (proj, mapClassToJava(depsPerJar(jar), rootProject.getAbsolutePathSrc(), proj))
    }

    // Initialise root project in target directory, if it did not already happen (Wrapper checks for initialised-property)
    copyGradleProjectWrapper(rootProject, repoPath, repo)

    // Add project as subproject in target and copy corresponding .java-files for every project
    filesPerJar.foreach(tuple => copyProject(tuple._1, tuple._2, repo))

    // Identify initialised subprojects that need to be in settings.gradle
    val initProjects = rootProject.listAllProjectsRecursively().filter(proj => proj.pathToGenTargetInRepo.keySet.contains(repo))

    // Add new subprojects from GitProject to TargetRootProject -> needs to happen at every addition of a new feature (at least check for necessity)
    val settingsFile = repoPath.resolve("settings.gradle")
    addSubprojectsToSettings(settingsFile, initProjects)

    filesPerJar
  }

  def extractAndImplantTestcase(subProject: Project, testCase: TestCase, insertionPoint: Tuple2[Asset, Int], originalToCloneMap: util.IdentityHashMap[Asset, Asset]): Tuple4[Asset, Asset, Feature, List[Operation]] = {
    val extractLocation = extractFileContent(subProject, testCase)

    val importFile = extractLocation.resolve("imports.java")
    val testcaseFile = extractLocation.resolve("testcase.java")

    val importContent = Files.readString(importFile)
    val testcaseContent = Files.readString(testcaseFile)

    val packageImport = s"import ${testCase.packageName}.*;\n"
    val adjustedImportContent = packageImport + importContent
    val adjustedTestcaseContent = s"try {\n$testcaseContent\n} catch (Exception e) {}\n"

    val testcaseBlockAsset = Asset("testcase", BlockType, content = Some(adjustedTestcaseContent.split('\n').toList))
    val testcaseBlockAssetClone = testcaseBlockAsset.copy()
    val feature = Feature(testCase.testName)
    val featureClone = feature.copy()

    // testcaseBlockAsset cannot just be added later, but needs to be recreated
    val addTestCaseOp = AddAssetAtPosition(testcaseBlockAsset, originalToCloneMap.get(insertionPoint._1), insertionPoint._2)
    val mapTestToFeatureOp = MapAssetToFeature(testcaseBlockAsset, feature)

    // find insertion point for import statements
    val fileParent = findFileParentAsset(insertionPoint._1).get

    // assuming first child of fileAsset contains package and import statements
    val importParent = originalToCloneMap(fileParent)
    val importAsset = Asset("testcaseimport", BlockType, content = Some(adjustedImportContent.split('\n').toList))
    val importBlockAssetClone = importAsset.copy()
    val addImportsOp = AddAssetAtPosition(importAsset, importParent, 1)

    (testcaseBlockAssetClone, importBlockAssetClone, featureClone, List(addTestCaseOp, mapTestToFeatureOp, addImportsOp))
  }


  // Idea: When converting files to VP-operations, only then check, if file already exists somewhere else -> dont clone entire folders, just files, as they are for now considered unmutable
  // but: is that even relevant for now? As I am not planning on doing anything with dependency-java-files anyways -> propagations will not be required?
  def createVPOps(currentPath: Path, parentAsset: Asset): Unit = {
    val assetType =
      if (Files.isDirectory(currentPath)) {
        FolderType
      }
      else {
        FileType
      }

    val newAsset = assetType match {
      case FolderType =>
        Asset(currentPath.toString, assetType)
      case FileType
        if currentPath.getFileName.toString.endsWith(".gradle") =>
        Asset(currentPath.toString, assetType, content = Some(Files.readString(currentPath).split('\n').toList))
      case FileType
        if currentPath.getFileName.toString.endsWith(".java") =>
        parseJavaFile(new File(currentPath.toAbsolutePath.toString), getImmediateAncestorFeatureModel(Some(parentAsset)).get)
      case FileType =>
        Asset(currentPath.toString, assetType)
    }
    AddAsset(newAsset, parentAsset)

    Files.walk(currentPath, 1)
      .filter(elem => !(elem == currentPath))
      .filter(elem => !ignoredPathNames.contains(elem.getFileName.toString))
      .forEach(elem => createVPOps(elem, newAsset))
  }

  def checkCreateVPOps(currentPath: Path, pathToAsset: Map[Path, Asset]): Unit = {
    Files.walk(currentPath, 1)
      .filter(elem => !(elem == currentPath))
      .filter(elem => !ignoredPathNames.contains(elem.getFileName.toString))
      .forEach(elem => {
        if (pathToAsset.keySet.contains(elem)) {
          checkCreateVPOps(elem, pathToAsset)
        }
        else {
          val parentAsset = pathToAsset(currentPath)
          createVPOps(elem, parentAsset)
        }
      })
  }

  // Idea: When converting files to VP-operations, only then check, if file already exists somewhere else -> dont clone entire folders, just files, as they are for now considered unmutable
  // but: is that even relevant for now? As I am not planning on doing anything with dependency-java-files anyways -> propagations will not be required?
  // pass parentRepoReroutePath as a workaround for an issue, where the lookup with the Asset reference does not work anymore, after new children were added to it -> idea: change structure to first recursive, then add to parent -> build bottom-up, this way repository gets a new child in the latest possible point
  def createVPOpsWithCloning(currentPath: Path, parentAsset: Asset, parentRepoReroutePath: Path, assetToPath: Map[Asset, Path]): Unit = {
    val assetType =
      if (Files.isDirectory(currentPath)) {
        FolderType
      }
      else {
        FileType
      }

    assetType match {
      case FolderType =>
        val newAsset = Asset(currentPath.toString, assetType)
        AddAsset(newAsset, parentAsset)
        Files.walk(currentPath, 1)
          .filter(elem => !(elem == currentPath))
          .filter(elem => !ignoredPathNames.contains(elem.getFileName.toString))
          .forEach(elem => createVPOpsWithCloning(elem, newAsset, parentRepoReroutePath, assetToPath))
      case FileType =>
        val relPath = currentPath.subpath(parentRepoReroutePath.getNameCount, currentPath.getNameCount)
        val potentialCloneOrigins = transformASTToList(rootAsset)
          .filter(asset => asset.assetType == RepositoryType && asset.name != findRepoOnUpPath(parentAsset).get.name)
          .map(repo => assetToPath(repo).resolve(relPath))
          .map(path => findAssetByName(path.toString, rootAsset))
          .flatten

        potentialCloneOrigins.length match {
          case 0 =>
            val newAsset = if (currentPath.getFileName.toString.endsWith(".gradle")) {
              Asset(currentPath.toString, assetType, content = Some(Files.readString(currentPath).split('\n').toList))
            } else if (currentPath.getFileName.toString.endsWith(".java")) {
              parseJavaFile(new File(file.Paths.get(System.getProperty("user.dir")).resolve(currentPath).normalize.toString), getImmediateAncestorFeatureModel(Some(parentAsset)).get)
            } else {
              Asset(currentPath.toString, assetType)
            }
            AddAsset(newAsset, parentAsset)
          case _ =>
            val cloneOrigin = potentialCloneOrigins(Random.nextInt(potentialCloneOrigins.length))
            CloneAsset(cloneOrigin, parentAsset)

        }
    }
  }

  def checkCreateVPOpsWithCloning(currentPath: Path, pathToAsset: Map[Path, Asset]): Unit = {
    Files.walk(currentPath, 1)
      .filter(elem => !(elem == currentPath))
      .filter(elem => !ignoredPathNames.contains(elem.getFileName.toString))
      .forEach(elem => {
        if (pathToAsset.keySet.contains(elem)) {
          checkCreateVPOpsWithCloning(elem, pathToAsset)
        }
        else {
          val parentAsset = pathToAsset(currentPath)
          val assetToPath = pathToAsset.map(elem => elem.swap)
          val parentRepoReroutePath = assetToPath(findRepoOnUpPath(parentAsset).get)
          createVPOpsWithCloning(elem, parentAsset, parentRepoReroutePath, assetToPath)
        }
      })
  }

  def resetProjectsIfNecessary(rootProject: Project, insertionRepo: Asset, beginInitProjects: Seq[Project]): Unit = {
    val endInitProjects = rootProject.listAllProjectsRecursively.filter(proj => proj.pathToGenTargetInRepo.keySet.contains(insertionRepo))
    val deltaInitProjects = endInitProjects.filter(proj => !beginInitProjects.contains(proj))
    deltaInitProjects.foreach(proj => proj.pathToGenTargetInRepo = proj.pathToGenTargetInRepo.filterNot(elem => elem._1 == insertionRepo))
  }

  def mapPathToAsset(project: Project, filePath: Path, repo: Asset) : Asset = {
    val assetPath = project.getAbsolutePathTarget(repo).get.resolve(project.srcSetMain).resolve(filePath)
    val asset = getassetbyname(assetPath.toString, repo).get
    asset
  }

  def mapFileDependenciesToFeature(insertionRepo: Asset, externalFileDependencies: Set[(Project, Seq[Path])], feature: Feature) : Unit = {
    for {
      filesPerProj <- externalFileDependencies
      project = filesPerProj._1
      fileDeps = filesPerProj._2
      filePath <- fileDeps
      asset = mapPathToAsset(project, filePath, insertionRepo)
    } yield {
      MapAssetToFeature(asset, feature)
    }
  }

  def extractTestcase(insertionPoint: Tuple2[Asset, Int], subProject: Project, testCase: TestCase): Option[AddExternalFeature] = {
    val cloner = new Cloner
    cloner.dontCloneInstanceOf(classOf[AssetType])
    val assetTreeCopy = cloner.deepClone(rootAsset)

    val originalAssetList = transformASTToList(rootAsset)
    val cloneAssetList = transformASTToList(assetTreeCopy)
    val originalToCloneAssetMap = new util.IdentityHashMap[Asset, Asset]()
    originalAssetList.zip(cloneAssetList).foreach(tuple => originalToCloneAssetMap.put(tuple._1, tuple._2))

    val insertionRepo = findRepoOnUpPath(insertionPoint._1).get
    val insertionRepoOldPath = file.Paths.get(insertionRepo.name)
    val insertionRepoRelPath = insertionRepoOldPath.subpath(readInPath.getNameCount - 1, insertionRepoOldPath.getNameCount)
    val insertionRepoPath = targetPath.resolve(insertionRepoRelPath)

    // For cleanup: Reset previously not initialised projects, if something goes wrong -> put this in both faulty cases
    val rootProject = subProject.getRootProject
    val beginInitProjects = rootProject.listAllProjectsRecursively().filter(proj => proj.pathToGenTargetInRepo.keySet.contains(insertionRepo))

    // seems to work
    val Tuple4(assetOriginal, assetImportOriginal, featureOriginal, vpOpList) = try {
      extractAndImplantTestcase(subProject, testCase, insertionPoint, originalToCloneAssetMap)
    } catch {
      case ex: Exception => println(s"Something went wrong during testcase extraction and implantation: ${ex.getMessage}")
        resetProjectsIfNecessary(rootProject, insertionRepo, beginInitProjects)
        throw TestcaseExtractionException(ex.getMessage)
    }

    cleanDirectory(targetPath, false)
    SerialiseAssetTree(assetTreeCopy, targetPath.toString)

    val externalFileDependencies = try {
      extractOutOfFileDependencies(insertionRepo, insertionRepoPath, subProject, testCase)
    } catch {
      case ex: Exception => println(s"Something went wrong during dependency management: ${ex.getMessage}")
        resetProjectsIfNecessary(rootProject, insertionRepo, beginInitProjects)
        throw DependencyManagementException(ex.getMessage)
    }

    // Add subproject dependency to root build-gradle.
    val rootBuildFile = insertionRepoPath.resolve("build.gradle")
    val projectDependency = ProjectDependency("implementation", subProject.getProjectPathIncludingRoot())
    addNewDependency(rootBuildFile, projectDependency)

    val compiled = executeGradle(insertionRepoPath.toString, "compileJava")
    println(compiled)

    if (compiled) {
      // Convert everything to VP-Operations on original assetTree and return TransplantFeatureOperation with some data such as the targeted Asset, Testcase and perhaps even a list with all operations for unapply?
      val List(add: AddAsset, map: MapAssetToFeature, imports: AddAssetAtPosition) = vpOpList
      val AddAssetAtPosition(_, addParentClone, assetPosition) = add
      val AddAssetAtPosition(_, importParent, importPosition) = imports

      val cloneToOriginalMap = new util.IdentityHashMap[Asset, Asset]()
      originalAssetList.zip(cloneAssetList).foreach(tuple => cloneToOriginalMap.put(tuple._2, tuple._1))
      originalToCloneAssetMap.keySet.forEach(key => assert(key == cloneToOriginalMap.get(originalToCloneAssetMap.get(key))))
      cloneToOriginalMap.keySet.forEach(key => assert(key == originalToCloneAssetMap.get(cloneToOriginalMap.get(key))))
      assert(cloneToOriginalMap.get(addParentClone) == insertionPoint._1)

      AddAssetAtPosition(assetOriginal, cloneToOriginalMap(addParentClone), assetPosition)
      AddAssetAtPosition(assetImportOriginal, cloneToOriginalMap(importParent), importPosition)
      MapAssetToFeature(assetOriginal, featureOriginal)
      MapAssetToFeature(assetImportOriginal, featureOriginal)

      val targetPathsToAssets = transformASTToList(rootAsset)
        .filter(asset => List(VPRootType, RepositoryType, FolderType, FileType).contains(asset.assetType))
        .map(asset => {
          // TODO: This is presumed problematic for CloneOperations -> need to fix asset.names after clones, so that they point to new location after clone (best in VP-implementation)
          // otherwise cloned assets will get added a second time by this step (or rather by checkCreateVPOps)
          val absPath = file.Paths.get(asset.name)
          val newAbsPath = if (absPath.startsWith(targetPath)) {
            // if asset.name begins with targetPath, the asset was added previously -> is this always true?
            absPath
          }
          else {
            // -1 for getNameCount, as it would otherwise crash for VPRootType as its absolute path = readInPath
            // since I still want to parse it, when writing assetTree to file structure I need to exclude the last layer (Root-level) from the path
            val relPath = absPath.subpath(readInPath.getNameCount - 1, absPath.getNameCount)
            targetPath.resolve(relPath)
          }
          (newAbsPath, asset)
        }).toMap
      val assetToTargetPath = targetPathsToAssets.map(elem => elem.swap)

      val rootPath = assetToTargetPath(rootAsset)
      checkCreateVPOpsWithCloning(rootPath, targetPathsToAssets)

      mapFileDependenciesToFeature(insertionRepo, externalFileDependencies, featureOriginal)

      val repoRootBuildFileAsset = insertionRepo.children.filter(asset => asset.name.endsWith("build.gradle")).head
      val repoRootSettingsFileAsset = insertionRepo.children.filter(asset => asset.name.endsWith("settings.gradle")).head
      val repoRootSettingsFile = insertionRepoPath.resolve("settings.gradle")
      ChangeAssetContent(repoRootBuildFileAsset, Files.readString(rootBuildFile).split('\n').toList)
      ChangeAssetContent(repoRootSettingsFileAsset, Files.readString(repoRootSettingsFile).split('\n').toList)

      // TODOs:
      // change containedPaths to be a dictionary of the adapted path and the corresponding asset
      // Implement FileWalk as Iteration through filesystem myself
      // OwnFilewalker should be recursive and iterate through each layer one-by-one (if no getChildren exists use walk with depth parameter set to 0 or 1)
      // Filewalker gets called with current path (starting with path of VPRootType-asset) and checks for every child, if child exists and if so -> recursive call, otherwise switch to recursiveAssetAdding with current corresponding Asset
      //    Check for current element: is element part of containedPaths.keys
      //      case yes => recursive call for next layer
      //      case no  => get parentasset by looking up asset in containedPaths-dictionary
      //                  call other function which recursively creates and adds Assets starting from this point

      // AssetAdding:
      // Access to current path and to-be-parentAsset
      // Create Asset
      // AddAsset to parentAsset
      // Call function recursively for children with freshly created Asset as new to-be-parentAsset

      //      val secondSerialisationPath = file.Paths.get("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TestTransplant_Original")
      //      cleanDirectory(secondSerialisationPath, false)
      //      SerialiseAssetTree(rootAsset, secondSerialisationPath.toString)
      Some(AddExternalFeature(repository = insertionRepo, insertionPoint = insertionPoint, project = subProject, testCase = testCase))
    }
    else {
      // reset projects that were initialised in this process
      resetProjectsIfNecessary(rootProject, insertionRepo, beginInitProjects)
      None
    }
  }
}

object AddFeatureGenerator {
  def apply(rootAsset: Asset, rootProjects: Seq[Project], targetPathString: String, readInPathString: String, filterInsertionPoints: Option[List[Asset]] = None): AddFeatureGenerator = new AddFeatureGenerator(rootAsset, rootProjects, targetPathString, readInPathString, filterInsertionPoints)
}