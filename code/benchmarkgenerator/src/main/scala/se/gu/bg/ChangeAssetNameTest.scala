package se.gu.bg

import se.gu.vp.model._
import se.gu.vp.operations._
import se.gu.bg.operations.ChangeAssetName

object ChangeAssetNameTest {
  def main(args: Array[String]): Unit = {
    var root = new Asset(name = "Root", assetType = VPRootType)
    var repo = new Asset("Repo", assetType = RepositoryType)
    var dir = new Asset(name =" Folder", FolderType)
    var file = new Asset("File", FileType)
    var func = new Asset("Function", MethodType)
    var block = new Asset("Block", BlockType)
    var block2 = new Asset("Block2", BlockType)

    AddAsset(asset = block2, parent = func)
    AddAsset(asset = func, parent = file)
    AddAsset(asset = block, parent = file)
    AddAsset(asset = file, parent = dir)
    AddAsset(asset = dir, parent = repo)
    AddAsset(asset = repo, parent = root)

    ChangeAssetName(dir, "NewFolder")
    ChangeAssetName(func, "NewFunc")
    ChangeAssetName(block, "NewBlock")

    root.prettyprint
  }

}