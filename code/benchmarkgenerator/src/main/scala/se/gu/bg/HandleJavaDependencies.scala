package se.gu.bg

import java.io.File
import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}

import scala.collection.JavaConversions._
import scala.sys.process
import scala.sys.process._
import Utilities.{copySafely, createDictIfNotExists, getAllIndicesOf}
import org.gradle.api.tasks.TaskExecutionException
import se.gu.bg.TestConfigurationProvider.{createDisruptorProj, createJadxProj, createReactiveXProj, createStructurizrProj, disruptorUtilProjTest, disruptorUtilTest, jadxCliRenameConverterProjTest, jadxCliRenameConverterTest, jadxProjTest, reactiveXDisposableObserverProjTest, reactiveXDisposableObserverTest, structurizrClientEncryptedJsonTests, structurizrClientStringUtilsTest}
import se.gu.bg.gradlehandler.GradleHandler.{adaptBuildGradle, getDependenciesAndClosureBorders, simplifyBuildGradle}
import se.gu.bg.model
import se.gu.bg.model.{Project, ProjectDependency}
import org.gradle.tooling.{GradleConnector, ProgressListener, ResultHandler}
import org.gradle.tooling.model.{GradleProject, ProjectModel}
import org.gradle.tooling.model.build.BuildEnvironment
import org.gradle.tooling.model.gradle.GradleBuild
import org.gradle.tooling.model.idea.IdeaDependency
import org.gradle.tooling.model.idea.IdeaModule
import org.gradle.tooling.model.idea.IdeaProject
import org.gradle.tooling.model.idea.IdeaSingleEntryLibraryDependency
import se.gu.vp.model.Asset

object HandleJavaDependencies {
// Not supported at the moment. Not migrated to OOP-style.
//  def getDependenciesRecursivelyWIndepClasspaths(classpathParam : String, rootProject: Path, classOrigin: Path, pathEnv: String, projectPath: Path, srcPath: Path, testPath: Path, depth: Int, recursivePath: List[String]): List[Tuple2[String,String]] =
//  {
//    // This is in order to be exchangeable with getDependenciesRecursively
//    val classpath = classpathParam.stripPrefix("\"").stripSuffix("\"")
//    classpath
//      .split(';')
//      .map(cp => getDependenciesRecursivelyOld("\""+cp+"\"", rootProject, projectPath, classOrigin, pathEnv, srcPath, testPath, depth, recursivePath))
//      .flatten
//      .distinct
//      .toList
//  }

  def getDependenciesRecursively(rootProject: Project, subProject: Project, testClass: Path, pathEnv: String, depth: Int, recursivePath: List[String]) : List[Tuple2[String,String]] = {
    def twoElArrayToTuple[T](arr: Array[T]) : Tuple2[T,T] =
    {
      assert(arr.length == 2)
      (arr(0),arr(1))
    }

    def checkClassDependency(strings: Array[String]) : Option[Array[String]] = {
      strings.length match {
        case 1 => None
        case 2
          if strings(0) == "not" && strings(1) == "found" =>
          None
        case 2 => Some(strings)
        case 3
          if strings(1) == "not" && strings(2) == "found" =>
          Some(Array(strings(0),"not found"))
        case default => None
      }
    }

    if (depth > 0) {
      throw new Exception("Recursive dependencies on other testclasses. This is not supported for now.")
    }

    val absProjectPath = subProject.getAbsolutePathSrc()
    val absClassOrigin = absProjectPath.resolve(testClass)

    // depth is just for debugging purposes
    println(depth + ": " + absClassOrigin)
    recursivePath.contains(absClassOrigin.toString) match {
      // recursivePath serves the purpose to catch dependency circles -> can probably happen, when there are multiple dependencies in one file, e.g. SuppressUndeliverableRule.class and SuppressUndeliverableRule$SuppressUndeliverableRuleStatement.class
      case true =>
        println("Dependency circle.")
        Nil
      case false =>
        // Classpath needs to start so that it can look for entire packagepath
        // D:\Dokumente\_Studium\masterarbeit\2021-vpbenchmark\testEnvironment\TestBase - Kopie>jdeps -verbose:class -recursive --class-path "build\classes\java\main" build\classes\java\main\com\hello\HelloWorld.class
        val classpathParam = rootProject.calcClasspath()
        val depString = Process(Seq("jdeps", "--class-path", classpathParam, "-recursive", "-verbose:class", absClassOrigin.toString),None,"PATH" -> pathEnv).!!

        // Convert output string of jdeps into a list of tuples representing all class-dependencies
        val dependencyList = depString
          // Split output into lines
          .split("\r\n")
          // Split lines at "->"
          .map(line => line.split("->"))
          // Check if part after "->" can be split into two parts to make a valid class-dependency instead of a jar-dependency -> maps to Some, if valid class-dep, None otherwise
          .map(line => (line(0).trim, checkClassDependency (line(1).split(' ').filter(elem => elem != ""))))
          // Filters out jar-dependencies, i.e. lines with only two elements (e.g. A -> B)
          .filter(line => line._2 != None)
          // Map to Tuple3
          .map(line => (line._1, twoElArrayToTuple (line._2.get)._1, twoElArrayToTuple (line._2.get)._2))

        // Elements in java base library should be available anyways and are thus not important here
        val reducedDependencyList = dependencyList
          .filter(dep => dep._3 != "java.base")
          .map(tuple3 => (tuple3._2,tuple3._3))

        // Convert classpathParam to classpath (strip "" around it)
        val classpath = classpathParam.stripPrefix("\"").stripSuffix("\"")
        // Assuming only .jars in classpath of jdeps -> TODO: check for .class-files
        val classpathJars = classpath
          .split(';')
          .map(path => path.split('\\').last)

        // Filter for all required elements in donor-project (those are local elements, which need to be copied)
        val reqElements = reducedDependencyList
          .filter(dep => classpathJars.contains(dep._2))

        // Filter for all elements not available in donor-project or base-library
        val unavailableElements = reducedDependencyList
          .filter(dep => !classpathJars.contains(dep._2))
          .map(dep => dep._1)

        val distinctReqElements = reqElements.distinct

        // TODO: Different way to handle giving path to compiled tests? -> could probably be made a member of Project?
        // Checks if "not found"-elements can be found in tests and recursively calls function for these test.classes
        // Can not utilise mapping.txt, as it maps JavaElements to .java-files, not to required .class-files for recursive calls [can this behaviour be assumed? assumes 1-to-1 mapping between java-namespaces and filestructure (for tests)]
        // Not a problem, if tests require only one .class-file, i.e. max(depth) = 0
        val testBeginPath = absProjectPath.resolve("build\\classes\\java\\test")
        val recursiveReqElements = for {
          element <- unavailableElements
          elementPath = testBeginPath.resolve(element.replace('.','/').concat(".class"))
          if (Files.exists(elementPath))
          requiredElements = getDependenciesRecursively(rootProject, subProject, elementPath, pathEnv, depth+1, absClassOrigin.toString :: recursivePath)
        } yield {
          requiredElements
        }

        val flatRecReqElements = recursiveReqElements.flatten

        val result = distinctReqElements ++ flatRecReqElements
        result.distinct.toList
    }
  }

  // supports only resolution of .class-files in same subproject
  def mapClassToJava(reqElements : Seq[Tuple2[String,String]], absRootProjectDir : file.Path, project : Project) : Seq[file.Path] = {
    // gets the line indices mapping to each .java-line
    def getRanges(keyIndices : Seq[Int], lineNumber : Int) : List[Tuple2[Int,List[Int]]] =
    {
      keyIndices match {
        case hd +: hd2 +: ls =>
          (hd, ((hd+1) until hd2).toList) :: getRanges(hd2 +: ls, lineNumber)
        case hd +: _ =>
          (hd, ((hd+1) until lineNumber).toList) :: Nil
        case _ => Nil
      }
    }

    val absProjectDir = project.getAbsolutePathSrc()

    // assuming mapping-file exists
    val mappingFile = absProjectDir.resolve("build/tmp/compileJava/source-classes-mapping.txt")

    // mapping is used for incremental compilation: https://github.com/gradle/gradle/blob/12a9731ad6415d0ae57d859b156f36a008d000a0/subprojects/language-java/src/main/java/org/gradle/api/tasks/compile/JavaCompile.java#L163
    // reading mapping in gradle implementation: https://github.com/gradle/gradle/blob/12a9731ad6415d0ae57d859b156f36a008d000a0/subprojects/language-java/src/main/java/org/gradle/api/internal/tasks/compile/SourceClassesMappingFileAccessor.java#L54
    // sourceFileClassNameConverter: https://github.com/gradle/gradle/blob/12a9731ad6415d0ae57d859b156f36a008d000a0/subprojects/language-java/src/main/java/org/gradle/api/internal/tasks/compile/incremental/recomp/DefaultSourceFileClassNameConverter.java#L26
    // otherwise: fileNameDerivingClassNameConverter: https://github.com/gradle/gradle/blob/12a9731ad6415d0ae57d859b156f36a008d000a0/subprojects/language-java/src/main/java/org/gradle/api/internal/tasks/compile/incremental/recomp/FileNameDerivingClassNameConverter.java#L25
    val fileContent = Files.readAllLines(mappingFile)
    val indexedFileContent =
      for {
        index <- 0 until fileContent.length
        line = fileContent(index)
      } yield {(index,line)}
    // filter out all indices for lines representing .java-files (all other begin with " ")
    val keyIndices = indexedFileContent.filter(element => !element._2.startsWith(" ")).map(element => element._1)
    // get list of mapped lines for each .java-line
    val keyRanges = getRanges(keyIndices, fileContent.length)
    // generate dictionary with the JavaElements being the keys mapping to the .java-files
    val depMapList = for {
      keyRange <- keyRanges
      mappingLineInd <- keyRange._2

      mapElem = fileContent(mappingLineInd).stripLeading()
      mapFile = fileContent(keyRange._1)
    } yield {
      (mapElem -> mapFile)
    }
    val depDict = depMapList.toMap

    val reqFiles = for (reqEl <- reqElements) yield (depDict(reqEl._1))
    // use distinct to have every required .java-file only once
    reqFiles.distinct.map(str => file.Paths.get(str))
  }


  // Returns optional rootProject-name, if set and a list of all included subprojects
  def parseSettings(settingsContent : String) : Tuple2[Option[String],Seq[String]] =
  {
    // rootProject is of type ProjectDescriptor: https://docs.gradle.org/current/javadoc/org/gradle/api/initialization/ProjectDescriptor.html
    // can theoretically be set using setName
    val projectRootNameRegex = "rootProject.name[\\s]*=[\\s]*'([^']+)'".r
    val rootNameLine = projectRootNameRegex findFirstIn settingsContent
    val rootProjectName = rootNameLine match {
      case None =>
        None
      case Some(line) =>
        val projectRootNameRegex(rootProjectNameMatch) = line
        Some(rootProjectNameMatch)
    }

    // https://docs.gradle.org/current/dsl/org.gradle.api.initialization.Settings.html#org.gradle.api.initialization.Settings:include(java.lang.String[])
    // only matches empty space notation, not brackets (groovy, not kotlin)
    val subprojectIncludeRegex = "include[\\s]+'([^']+)'".r
    val includedSubprojectLineIterator = for {
      includedSubprojectMatch <- subprojectIncludeRegex findAllIn settingsContent
      subprojectIncludeRegex(includedSubproject) = includedSubprojectMatch
    } yield { includedSubproject }
    val subprojects = includedSubprojectLineIterator.toList
    (rootProjectName, subprojects)
  }

  def getProjectNameAndSubprojects(path : Path, settingsFile : Path) : Tuple2[String,Seq[String]] =
  {
    val (intmedProjName, subprojects) =
      if (Files.exists(settingsFile))
      {
        val settings = Files.readString(settingsFile)
        parseSettings(settings)
      }
      else
      {
        (None,Nil)
      }
    intmedProjName match {
      case None => (path.getFileName.toString,subprojects)
      case Some(name) => (name, subprojects)
    }
  }

  // When called for subproject, assumes that targetDir is set to absolute path of target root project
  def copyGradleProjectWrapper(project: Project, targetDir : Path, repo: Asset) : Unit =
    {
      project.pathToGenTargetInRepo.contains(repo) match {
        case true =>
        case false =>
          project.parent match {
            case None =>
              val targetPath = targetDir.resolve(project.name)
              copyGradleProject(project, targetPath, None, repo)
              project.pathToGenTargetInRepo = project.pathToGenTargetInRepo ++ Map((repo, targetPath))
            case _ =>
              copyGradleProject(project,targetDir,None, repo)
              project.pathToGenTargetInRepo = project.pathToGenTargetInRepo ++ Map((repo,file.Paths.get(project.name)))
          }
      }
    }

  def copyGradleProject(project : Project, targetPath : Path, dirName : Option[String], repo: Asset) =
  {
      val projectPath = project.getAbsolutePathSrc()

      // File walk heuristic -> w/o elements in subprojects
      // Take all elements not part of a subproject, that have gradle in their name (just .gradle would not capture gradle.properties) -> hope to combat missing files (referenced files, e.g. configuration files can still be missing (https://docs.gradle.org/current/userguide/custom_plugins.html#sec:working_with_files_in_custom_tasks_and_plugins))
      // should capture build.gradle, settings.gradle, gradle.properties, convention plugins, e.g. myproject.java-conventions.gradle [https://docs.gradle.org/current/samples/sample_convention_plugins.html], script plugins [https://docs.gradle.org/current/userguide/plugins.html#sec:script_plugins , https://docs.gradle.org/current/userguide/custom_plugins.html#sec:precompiled_plugins]
      // note that convention plugins in the showcased way are script plugins (see example in: https://docs.gradle.org/current/userguide/custom_plugins.html#sec:precompiled_plugins)
      // three options for source of plugins: inside build script (-> build.gradle?), buildSrc (script plugin (does not need to be .gradle, can be in different langauges)), standalone project (import jar from some repository or have it locally?) (https://docs.gradle.org/current/userguide/custom_plugins.html#sec:packaging_a_plugin)
      // TODO: check API for applying plugins (-> from where)
      // build.gradle is not filtered out, as it is saved to target before the original would be copied. copySafely only copies, if target path does not yet exist.
//      val elements: Array[Path] = Files.walk(projectPath)
//        .filter(file => !project.subprojects.exists(subproject => file.startsWith(subproject.getAbsolutePathSrc())))
//        .filter(file => file.toString.contains("gradle"))
//        //.filter(file => file.getFileName.toString.contains("gradle"))
//        .map(path => projectPath.relativize(path))
//        // not sure how to do this in scala
//        .toArray
//        .map(obj => obj.asInstanceOf[Path])

      // Get path to project and create directory in case it does not exist already
      // Assume no settings.gradle in subprojects or at least no set rootProjectName different from folder <- still relevant after refactoring?
      val targetProject = targetPath.resolve(project.getRelativePathFromRoot())
      Files.createDirectories(targetProject)

      // Opens build.gradle, if it exists, simplifies it, adapts it to the new project structure and writes it to target location. Otherwise: nothing.
      // Assumes build.gradle to be in top-level dir of project -> TODO: CHECK: Can it in theory even be otherwise?
      val buildFile = projectPath.resolve("build.gradle")
      Files.exists(buildFile) match {
        case false =>
        case true =>
          val buildFileContent = Files.readString(buildFile)
          val simplifiedBuildFileContent = simplifyBuildGradle(buildFileContent)
          val adaptedBuildFileContent = adaptBuildGradle(simplifiedBuildFileContent, project)
          //val adaptedBuildFileContent = buildFileContent
          val file = targetProject.resolve("build.gradle")
          Files.writeString(file, adaptedBuildFileContent)

          // Extract project dependencies and initialises them
          val depRegEx = raw"dependencies\s*\Q{\E".r
          val depMatches = depRegEx.findAllMatchIn(adaptedBuildFileContent).toList
          //val depBegins = getAllIndicesOf(adaptedBuildFileContent,"dependencies")

          val dependencies = for {
            // Reverse the dependencies to not mess up the indexing of dependency-blocks with a higher identified starting index
            depMatch <- depMatches.reverse
            (dependencies,_,_) = getDependenciesAndClosureBorders(adaptedBuildFileContent,depMatch.start)
          } yield { dependencies }

          val dependentProjects = dependencies.flatten
            .filter(dep => dep match {
              case _ : ProjectDependency => true
              case _ => false
            })
            .map(dep => project.getRootProject().getSubprojectByName(dep.dependency.stripPrefix(s":${project.getRootProject().name}:")).get)

          for {
            dependentProject <- dependentProjects
          } yield {
            copyGradleProjectWrapper(dependentProject, targetPath, repo)
          }
      }

      // Copy all "gradle"-related files
      //elements.foreach(src => copySafely(projectPath.resolve(src), targetProject.resolve(src)))

      // Basically a split between RootProject and Project, where RootProject contains absolute paths, but you can get the relative path, i.e. "" via getRelativePathFromRoot().
      // Additionally, name and srcPath can be different whereas Project contains only the relative path from its immediate parent-project and its folder names are limited to be equal to its project name.

  }

  // rootProjectPath: Absolute path to root project
  // subProjectPath: Relative path from root project to subproject (assuming only one layer for now)
  // javaFiles: List of all java-files that need to be copied
  // targetPath: Path to corresponding rootProject-folder in target
  // srcPath: Project-specific path from project-folder to source-code
  // Plan:
  // 1. Resolve rootProjectPath and targetPath with subProjectPath to get the absolute paths to the subproject
  // 2. Use CopyGradleProject to initialise folder as gradle project with build-files and other gradle related files
  // 3. Attach srcPath to absolute subprojectpath in src and target
  // 4. Copy java-files
  // 5. ???
  // 6. PROFIT
  def copyProject(project: Project, javaFiles: Seq[Path], repo: Asset) : Unit  =
  {
    val rootProject = project.getRootProject()

    // copyGradleProjectWrapper assumes to get the rootProjects targetpath for any non-root project
    val rootTargetPath = rootProject.pathToGenTargetInRepo(repo)

    val parentProjects = project.getParentProjects()
    parentProjects.foreach(proj => copyGradleProjectWrapper(proj, rootTargetPath, repo))

    copyGradleProjectWrapper(project, rootTargetPath, repo)
    // Assumes, that either no settings.gradle is present in subproject or rootProject.name therein is not set to a name, different from folder name in parent-folder
    // This is a weaker assumption, than that all sub(!)projects have identical name and path
    // TODO: Unify this for different projects (related to upper TODO) -> assertion is wrong due to case of empty subProjectPath (rootDir of GitProject) and nested project names, e.g. in jadx-case
    // assert(projName == subProjectPath.getFileName.toString)

    val gitSubProjectPath = project.getAbsolutePathSrc()
    val gitSubProjectCodePath = gitSubProjectPath.resolve(project.srcSetMain)
    val targetSubProjectCodePath = project.getAbsolutePathTarget(repo).get.resolve(project.srcSetMain)

    javaFiles.foreach(file => copySafely(gitSubProjectCodePath.resolve(file),targetSubProjectCodePath.resolve(file)))
  }

  // https://docs.gradle.org/6.8.2/dsl/org.gradle.api.initialization.Settings.html#org.gradle.api.initialization.Settings:include(java.lang.String[])
  def addSubprojectsToSettings(settingsFile : Path, projects : Seq[Project]) : Unit =
  {
    // TODO: Create settings.gradle, if it does not exist
    if (!Files.exists(settingsFile))
      {
        throw new Exception("settings.gradle does not exist on repository layer.")
      }

    val settingsContent = Files.readString(settingsFile)                                                                                                                                                                                                                                                         

    // Checks if projects are included via :a:b or a:b and includes them otherwise
    val linesToAppend = for {
      proj <- projects
      // Get complete project path -> probably needs to get adjusted for nesting donor-projects
      projPath = proj.getProjectPathIncludingRoot()
      // Check if project already exists (two notations: with or without leading ':')
      if !settingsContent.contains(s"include '$projPath'") && !settingsContent.contains(s"include ':$projPath'")
    } yield {
      s"include '$projPath'"
    }

    // Append lines for missing projects into settings.gradle
    Files.write(settingsFile,linesToAppend, StandardOpenOption.APPEND)
  }

  def main(args: Array[String]): Unit =
  {
    //--- Initialisation

    //val target = file.Paths.get("..\\testEnvironment\\TestBase_newConfigtest\\")
    val target = file.Paths.get("..\\testEnvironment\\TestBase\\")
    assert(Files.exists(target))

    // Previously in copyGradleProject -> could be useful for initialising Project-Structure
    //        //Get names of project and subprojects
    //        //TODO: look into rootProject.name -> documentation
    //        val (projName, subprojects) = getProjectNameAndSubprojects(projectPath, settingsFile)
    //
    //        // https://docs.gradle.org/current/dsl/org.gradle.api.initialization.Settings.html#org.gradle.api.initialization.Settings:include(java.lang.String[])
    //        // assume subprojects are mapped directly to same named folders! -> default, if not changed by projectDir is not changed (happens e.g. in Mockito)
    //        // specified subprojects can have a leading ':' (tested manually), strip it and take the distinct toplevel-subprojects
    //        val topLevelSubProjects = subprojects.map(sp => file.Paths.get(sp.stripPrefix(":").split(':')(0))).distinct


    // Setting up parameters -> old way for testing purposes
    //val (classpath1,classPathClass1,rootProjectPath1,originProjectPath1,classOrigin1,srcPath1,testPath1, projectDirToJarMap1, jarToProjectDirMap1, path1) = structurizrClientStringUtilsTest()
    //val (classpath1,rootProjectPath1,originProjectPath1,classOrigin1,srcPath1,testPath1, projectDirToJarMap1, jarToProjectDirMap1, path1) = reactiveXDisposableObserverTest()
    //val (classpath1,rootProjectPath1,originProjectPath1,classOrigin1,srcPath1,testPath1, projectDirToJarMap1, jarToProjectDirMap1, path1) = jadxCliRenameConverterTest()
    //val (classpath1,rootProjectPath1,originProjectPath1,classOrigin1,srcPath1,testPath1, projectDirToJarMap1, jarToProjectDirMap1, path1) = disruptorUtilTest()

    // Project is loaded
    val rootProject = createStructurizrProj()
    //val rootProject = createReactiveXProj()
    //val rootProject = createJadxProj()
    //val rootProject = createDisruptorProj()

    // Concrete Testclass to transplant
    val (subProject,classOrigin,path) = structurizrClientEncryptedJsonTests(rootProject,"structurizr-client")
    //val (subProject,classOrigin,path) = reactiveXDisposableObserverProjTest(rootProject,"")

    // jadx
    //val (subProject,classOrigin,path) = jadxCliRenameConverterProjTest(rootProject,"jadx-cli")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"jadx-cli","build\\classes\\java\\test\\jadx\\cli\\TestInput.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"jadx-core","build\\classes\\java\\test\\jadx\\tests\\functional\\AttributeStorageTest.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"jadx-core","build\\classes\\java\\test\\jadx\\core\\xmlgen\\entry\\ValuesParserTest.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"jadx-core","build\\classes\\java\\test\\jadx\\api\\JadxDecompilerTest.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"jadx-gui","build\\classes\\java\\test\\jadx\\gui\\treemodel\\JSourcesTest.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"jadx-gui","build\\classes\\java\\test\\jadx\\gui\\utils\\CertificateManagerTest.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"jadx-plugins:jadx-dex-input","build\\classes\\java\\test\\jadx\\plugins\\input\\dex\\DexInputPluginTest.class")

    // Structurizr
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"structurizr-core","build\\classes\\java\\test\\com\\structurizr\\model\\ComponentTests.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"structurizr-client","build\\classes\\java\\test\\com\\structurizr\\api\\ApiResponseTests.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"structurizr-client","build\\classes\\java\\test\\com\\structurizr\\api\\StructurizrClientTests.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"structurizr-client","build\\classes\\java\\test\\com\\structurizr\\encryption\\EncryptedWorkspaceTests.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"structurizr-client","build\\classes\\java\\test\\com\\structurizr\\api\\StructurizrClientIntegrationTests.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"structurizr-client","build\\classes\\java\\test\\com\\structurizr\\encryption\\AesEncryptionStrategyTests.class")

    // disruptor
    //val (subProject,classOrigin,path) = disruptorUtilProjTest(rootProject,"")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"","build\\classes\\java\\test\\com\\lmax\\disruptor\\SequenceGroupTest.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"","build\\classes\\java\\test\\com\\lmax\\disruptor\\FatalExceptionHandlerTest.class")
    //val (subProject,classOrigin,path) = jadxProjTest(rootProject,"","build\\classes\\java\\test\\com\\lmax\\disruptor\\DisruptorStressTest.class")

    // Get Map from jar-names to their corresponding projects, assumes unique jar-names.
    val jarPathToProjectMap = rootProject.getJarToProjMap()
    val jarNameToProjectMap = jarPathToProjectMap.map(tuple => (tuple._1.getFileName.toString,tuple._2))

//    val testExtractorPath = file.Paths.get("D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\UnitTestScribe\\WM.UnitTestScribe\\bin\\Release\\TeaCap.exe")
//    val loc = subProject.getAbsolutePathSrc().resolve(subProject.srcSetTest).toString
//    val result = Process(Seq(testExtractorPath.toString,
//      "extractTestcase",
//      "--loc", loc,
//      "--srcmlPath", "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\UnitTestScribe\\SrcML",
//      "--outputLoc", "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases",
//      "--namespace", "com.structurizr.api",
//      "--class", "StructurizrClientTests",
//      "--test", "test_getWorkspace_ThrowsAnException_WhenTheWorkspaceIdIsNotValid"
//    )).!!

    //Test Gradle Tooling API
    val connection = GradleConnector.newConnector.forProjectDirectory(new File(rootProject.getAbsolutePathSrc().toString)).connect

    // Seems to contain most things I needed apart from repositories, not sure about sourceSets
    val project = connection.getModel(classOf[IdeaProject])
//    val modules = project.getModules
//    for (module <- project.getModules) {
//      for (dependency <- module.getDependencies) {
//        val ideaDependency = dependency.asInstanceOf[IdeaSingleEntryLibraryDependency]
//        val file = ideaDependency.getFile
//      }
//    }
    val listener = GradleProgressListener()

    connection.newBuild().forTasks("projects").run()
    connection.action().build().run()
    // when build fails -> exception is thrown -> Listener should not be necessary, just try catch and if caught -> compilation failed
    try {
      connection.newBuild().forTasks("clean", "compileJava").addProgressListener(listener).run()
    } catch {
      case ex : TaskExecutionException => println (s"TaskExecutionException: ${ex.getMessage}")
      case ex : Exception => println(s"Ordinary Exception: ${ex.getMessage}")
      case _ => println("Not an exception, still caught.")
    }
    connection.action().build().forTasks("jar","compileTest").run()
    //.addProgressListener(new ProgressListener())
        val buildEnv = connection.getModel(classOf[BuildEnvironment])
    val gradBuild = connection.model[GradleBuild](classOf[GradleBuild])
    val gradProjBuilder = connection.model[GradleProject](classOf[GradleProject])
    val model = gradProjBuilder.get()
    val projModel = connection.getModel(classOf[ProjectModel])
    try connection.newBuild.forTasks("tasks").run
    finally {
      connection.close
    }


    //--- TransplantProcess starts here
    // Gets all required .class-files together with their containing .jar-files
    // Is logically the same once the recursive call in getDependenciesRecursively depends on the mapping.txt-file (WIndepClasspaths is less performant the bigger and deeper the project is)
    val elements  = getDependenciesRecursively(rootProject,subProject,classOrigin,path,0,Nil)
    //val elementsWIndCP = getDependenciesRecursivelyWIndepClasspaths(classpath, classOrigin, path, originProjectPath, srcPath, testPath,0, Nil)
    //assert(elements.sorted.sameElements(elementsWIndCP.sorted))

    // Test jdeps with .class ->
    // issue might be no clear mapping from different src/java/main files from different projects -> either requires different final folder names or suffers in terms of performance -> for each classes-folder on classpath -> no mapping between .jar and project, just iterate over all projects and take classes-folder
    // other resolution for issue: temporarily rename folders? while gradle has no access?
    //    val elements2 = getDependenciesRecursively(classPathClass, rootProjectPath, originProjectPath, classOrigin, path, srcPath, testPath, 0, Nil)
    //    val elements1 = elements.map(m => m._1).sorted
    //    val elements21 = elements.map(m => m._1).sorted
    //    assert(elements1 == elements21)

    // Maps the list of .class-files per .jar to a list of .java-files per Project
    val depsPerJar = elements.groupBy(el => el._2)
    val filesPerJar = for {
      jar <- depsPerJar.keySet
      proj = jarNameToProjectMap(jar)
    } yield {
      (proj, mapClassToJava(depsPerJar(jar), rootProject.getAbsolutePathSrc(), proj))
    }

//    // Initialise root project in target directory, if it did not already happen (Wrapper checks for initialised-property)
//    copyGradleProjectWrapper(rootProject, target)
//
//    // Add project as subproject in target and copy corresponding .java-files for every project
//    filesPerJar.foreach(tuple => copyProject(tuple._1, tuple._2))
//
//    // Identify initialised subprojects that need to be in settings.gradle
//    val initProjects = rootProject.listAllProjectsRecursively().filter(proj => proj.initialised)
//
//    // Add new subprojects from GitProject to TargetRootProject -> needs to happen at every addition of a new feature (at least check for necessity)
//    val settingsFile = target.resolve("settings.gradle")
//    addSubprojectsToSettings(settingsFile,initProjects)
    Nil
  }
}
