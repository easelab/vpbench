package se.gu.bg.generators

import se.gu.vp.model._
import se.gu.vp.operations.RemoveAsset
import se.gu.vp.operations.Utilities._

import scala.util.Random

//TODO: test implementierung von unapply für Operations

class RemoveAssetGenerator(val rootAsset:Asset) extends Generator{
  override def generate: Option[RemoveAsset] = {
    val assets = transformASTToList(rootAsset)
    val filteredAssets = assets.filter(a => a.assetType != VPRootType)

    val removedAsset = filteredAssets(Random.nextInt(filteredAssets.length))

    // for debugging
    println(s"RemoveAsset $removedAsset")

    val generated = new RemoveAsset(rootAsset, assetToDelete = Some(removedAsset))
    Some(generated)
  }
}

object RemoveAssetGenerator {
  def apply(rootAsset:Asset) : RemoveAssetGenerator = new RemoveAssetGenerator(rootAsset)
}