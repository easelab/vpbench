package se.gu.bg

import java.io.File
import java.nio.file
import java.nio.file.{Files, Path}
import java.text.SimpleDateFormat
import java.util.Calendar

import se.gu.bg.TestConfigurationProvider.{createAlgorithmsProj, createDisruptorProj, createEventbusProj, createJadxProj, createJsonJavaProj, createNLPProj, createReactiveXProj, createStructurizrProj, createTeammatesProj}
import se.gu.bg.generators.{AddFeatureGenerator, AddLineGenerator, DeleteLineGenerator, Generator, RemoveFeatureGenerator, ReplaceLineGenerator}
import se.gu.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.SerialiseAssetTree
import se.gu.vp.operations.Utilities.{getassetbyname, transformASTToList}
import net.liftweb.json._
import net.liftweb.json.Serialization.write
import se.gu.bg.evaluation.{AggregatedResults, Parameters}
import se.gu.vp.model.{Asset, BlockType, FileType}
import se.gu.bg.Utilities.isTransitiveChildOf

import scala.sys.process.Process

object RunEvaluation {

  val clocPath = "D:\\Downloads\\sqlite-tools-win32-x86-3350500\\sqlite-tools-win32-x86-3350500\\cloc-1.88.exe"
  val sqlitePath = "D:\\Downloads\\sqlite-tools-win32-x86-3350500\\sqlite-tools-win32-x86-3350500\\sqlite3.exe"

  def saveLoC(experimentPath: Path, numIterations: Int) = {
    val dbInitScript = Process(Seq(clocPath, "--sql", "1", "--sql-project", "0", experimentPath.resolve("v0").toString), None,"PATH" -> "").!!
    Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + dbInitScript +"\""), None, "PATH" -> "").!
    for (iteration <- 1 to numIterations) {
      val additionScript = Process(Seq(clocPath, "--sql", "1", "--sql-project", s"$iteration", "--sql-append", experimentPath.resolve(s"v$iteration").toString), None,"PATH" -> "").!!
      val tempFile = Files.createTempFile(experimentPath, null, ".sql")
      Files.writeString(tempFile, additionScript)

      Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\".read", tempFile.toString.replace("\\", "\\\\"), "\""), None, "PATH" -> "").!
    }
    val csvFile = experimentPath.resolve("loc.csv")
    val sqlQuery = "\"select project as iteration, sum(nCode) as loc from t where t.language = 'Java' group by project, language\""
    val csvContent = Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "-header", "-csv", sqlQuery), None, "PATH" -> "").!!
    Files.writeString(csvFile, csvContent)
  }

  def postprocessJavaParsing(ast: Asset, insertionPointLimitations: List[Asset]) : Unit = {
    val assetsToProcess = transformASTToList(ast)
      .filter(asset => asset.assetType == FileType)
      .filter(asset => isTransitiveChildOf(asset, insertionPointLimitations))

    for (asset <- assetsToProcess) {
      if (asset.children.length != 0) {
        val currentBlock = asset.children(0)
        val packageBlock = Asset("package", BlockType, content = Some(currentBlock.content.get(0) :: Nil))
        val secondBlock = Asset("block", BlockType, content = Some(currentBlock.content.get.drop(1)))
        packageBlock.parent = Some(asset)
        secondBlock.parent = Some(asset)

        asset.children = packageBlock :: secondBlock :: asset.children.drop(1)
      }
    }
  }

  def main(args: Array[String]) : Unit =
  {
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm")
    val datetime = Calendar.getInstance().getTime()
    val targetpath =  "D:\\Dokumente\\_Studium\\masterarbeit\\evaluation"
    val targetdatepath = targetpath + "\\" + dateFormat.format(datetime)
    val targetdatedir = new File(targetdatepath)
    targetdatedir.mkdir()

    val iterations = 1000
    val initialProjects = List(
      ("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot",
        List("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\CalculatorRepo\\src\\main\\java")),
      ("D:\\Dokumente\\_Studium\\masterarbeit\\gitClones\\JSON-java\\JSON-javaRoot",
        List("D:\\Dokumente\\_Studium\\masterarbeit\\gitClones\\JSON-java\\JSON-javaRoot\\JSON-java\\src\\main\\java"))
      )
    val probDistributions = List((1, Array(0.11111,0.11111,0.11111,0.33333,0.33333)),(2, Array(0.2,0.2,0.2,0.2,0.2)),(3, Array(0.2,0.2,0.2,0.3,0.1)))
    val maxAttempts = 50
    val discardUselessChangesProb = 0.5

    val results = for {
      (readInPath, insertionPointLimitations) <- initialProjects
      (probDistributionNumber, probDistribution) <- probDistributions
      numIterations = iterations
    } yield {
      val ast = readInJavaAssetTree(readInPath).get
      val filterInsertionPoints = insertionPointLimitations.map(name => getassetbyname(name, ast).get)
      postprocessJavaParsing(ast, filterInsertionPoints)

      // Initialise Transplantation Projects
      val structurizrProject = createStructurizrProj
      val disruptorProject = createDisruptorProj
      val rootProjects = List(structurizrProject, disruptorProject)

      val addLineGenerator = AddLineGenerator(ast, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath, discardUselessChangesProb)
      val repLineGenerator = ReplaceLineGenerator(ast, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath, discardUselessChangesProb)
      val delLineGenerator = DeleteLineGenerator(ast, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath, discardUselessChangesProb)
      val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath, Some(filterInsertionPoints))
      val remFeatureGenerator = RemoveFeatureGenerator(ast, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath)
      val generators = List(addLineGenerator, repLineGenerator, delLineGenerator, addFeatureGenerator, remFeatureGenerator)
      val config : Map[Generator, Double] = generators.zip(probDistribution).toMap

      val runner = new BenchmarkGenerationRunner(ast, generators, Some(config))

      val initialProjectName = readInPath.split("\\\\").last

      val relPath = s"numIt_${numIterations}_${initialProjectName}_$probDistributionNumber"
      val experimentPath = file.Paths.get(targetdatedir.getAbsolutePath).resolve(relPath)
      Files.createDirectories(experimentPath)

      val paramsFilePath = experimentPath.resolve("params.json")
      Files.createFile(paramsFilePath)
      val parameters = Parameters(numIterations, readInPath, insertionPointLimitations, probDistributionNumber, probDistribution, maxAttempts, discardUselessChangesProb)
      implicit val formats = DefaultFormats
      val jsonString = write(parameters)
      Files.writeString(paramsFilePath, jsonString)

      val timestampBegin = System.nanoTime
      val results = runner.run(numIterations, experimentPath.toString, None, maxAttempts = maxAttempts)
      val timestampEnd = System.nanoTime
      val duration = timestampEnd - timestampBegin

      // Only one repository is used (otherwise differentiate between features in repo and entire system -> would need update in Runner)
      val numFeaturesOverTime = results.map(iterationResult => (iterationResult.iteration, iterationResult.repoResults(0).numFeatures))
      val scatteredFeaturesOverTime = results.map(iterationResult => (iterationResult.iteration, iterationResult.repoResults(0).scatterDegPerFeature.map(tuple => tuple._2)))
      val tangledFeaturesOverTime = results.map(iterationResult => (iterationResult.iteration, iterationResult.repoResults(0).tangleDegPerFeature.map(tuple => tuple._2)))

      val numFeaturesOverTimeCsv = "iteration, numFeatures" :: numFeaturesOverTime.map(it => s"${it._1}, ${it._2}")
      val numFeaturesCsvPath = experimentPath.resolve("numFeatures.csv")
      Files.createFile(numFeaturesCsvPath)
      Files.writeString(numFeaturesCsvPath, numFeaturesOverTimeCsv.mkString("\n"))

      val scatteringDegreesOverTime = scatteredFeaturesOverTime
        .filter(it => it._2.length != 0)
        .map(it => (it._1, (it._2.sum.asInstanceOf[Double] / it._2.length.asInstanceOf[Double]), it._2.mkString(";")))
      val scatteredFeaturesOverTimeCsv = "iteration, avgScatDeg, scatDegs" :: scatteringDegreesOverTime.map(it => s"${it._1}, ${it._2}, ${it._3}")
      val scatteredFeaturesCsvPath = experimentPath.resolve("scatFeatures.csv")
      Files.createFile(scatteredFeaturesCsvPath)
      Files.writeString(scatteredFeaturesCsvPath, scatteredFeaturesOverTimeCsv.mkString("\n"))

      val tanglingDegreesOverTime = tangledFeaturesOverTime
        .filter(it => it._2.length != 0)
        .map(it => (it._1, (it._2.sum.asInstanceOf[Double] / it._2.length.asInstanceOf[Double]), it._2.mkString(";")))
      val tangledFeaturesOverTimeCsv = "iteration, avgTangDeg, tangDegs" :: tanglingDegreesOverTime.map(it => s"${it._1}, ${it._2}, ${it._3}")
      val tangledFeaturesCsvPath = experimentPath.resolve("tangFeatures.csv")
      Files.createFile(tangledFeaturesCsvPath)
      Files.writeString(tangledFeaturesCsvPath, tangledFeaturesOverTimeCsv.mkString("\n"))

      val locdb = experimentPath.resolve("loc.db")
      Files.createFile(locdb)
      saveLoC(experimentPath, numIterations)

      val resultsFilePath = experimentPath.resolve("aggregatedStatistics.json")
      val numCompiledVersions = results.filter(it => it.repoResults(0).compiled).length
      val numChanges = results.filter(it => it.changed).length
      val aggStats = AggregatedResults(duration, numberOfCompiledIterations = numCompiledVersions, numberOfAppliedChanges = numChanges)
      val resultsString = write(aggStats)
      Files.writeString(resultsFilePath, resultsString)
    }
  }
}
