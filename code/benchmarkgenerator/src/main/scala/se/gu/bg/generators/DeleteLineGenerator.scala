package se.gu.bg.generators

import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}
import java.util
import java.util.Comparator

import com.rits.cloning.Cloner
import se.gu.bg.Utilities.{cleanDirectory, findRepoOnUpPath}
import se.gu.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.bg.operations.ChangeAssetContent
import se.gu.vp.model.{Asset, AssetType}
import se.gu.vp.operations.{ChangeAsset, RemoveFeature, SerialiseAssetTree}
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath, transformASTToList}

import scala.util.Random
import scala.collection.JavaConversions

class DeleteLineGenerator(override val rootAsset: Asset, targetPathString: String, readInPathString: String, uselessChangeDiscardProb: Double) extends ChangeAssetGenerator{
  override val targetPath: Path = file.Paths.get(targetPathString)
  override val readInPath: Path = file.Paths.get(readInPathString)

  // identifies empty lines and single line comments
  val lineUsefulnessRegex = "\\s*(//.*)?".r

  override def changeAsset(asset: Asset): Option[ChangeAsset] = {
    val cloner = new Cloner
    cloner.dontCloneInstanceOf(classOf[AssetType])
    cloner.registerConstant(None)
    val assetTreeCopy = cloner.deepClone(rootAsset)

    val originalAssetList = transformASTToList(rootAsset)
    val cloneAssetList = transformASTToList(assetTreeCopy)
    val originalToCloneAssetMap = new util.IdentityHashMap[Asset, Asset]()
    originalAssetList.zip(cloneAssetList).foreach(tuple => originalToCloneAssetMap.put(tuple._1, tuple._2))

    val cloneSelectedAsset = originalToCloneAssetMap.get(asset)

    if (asset.content == None || asset.content.get.length == 0) {
      None
    } else {
    val content = asset.content.get

    val indexedContent = for {
        index <- 0 until content.length
        line = content(index)
      } yield {
      (index,line)
    }

    val deleteLineIndex = Random.nextInt(content.length)
    val deleteLine = content(deleteLineIndex)
      if (deleteLine.matches(lineUsefulnessRegex.regex)) {
        if (Random.nextDouble <= uselessChangeDiscardProb) {
          return None
        }
      }

    val newContent = indexedContent
      .filter(tuple => tuple._1 != deleteLineIndex)
      .sortBy(tuple => tuple._1)
      .toList
      .map(tuple => tuple._2)

    ChangeAssetContent(cloneSelectedAsset, newContent)

      cleanDirectory(targetPath, false)
      SerialiseAssetTree(assetTreeCopy, targetPath.toString)

      val affectedRepository = findRepoOnUpPath(asset).get
      val affectedRepoOldPath = file.Paths.get(affectedRepository.name)
      val affectedRepoRelPath = affectedRepoOldPath.subpath(readInPath.getNameCount - 1, affectedRepoOldPath.getNameCount)
      val affectedRepoPath = targetPath.resolve(affectedRepoRelPath)

      val compiled = executeGradle(affectedRepoPath.toString, "compileJava")
      println(compiled)

      if (compiled) {
        println(s"Deleted line: ${indexedContent(deleteLineIndex)._2}")
        Some(ChangeAssetContent(asset, newContent))
      } else {
        None
      }
    }
  }
}

object DeleteLineGenerator {
  def apply(rootAsset:Asset, targetPathString: String, readInPathString: String, uselessChangeDiscardProb: Double = 0.0) : DeleteLineGenerator = new DeleteLineGenerator(rootAsset, targetPathString, readInPathString, uselessChangeDiscardProb)
}