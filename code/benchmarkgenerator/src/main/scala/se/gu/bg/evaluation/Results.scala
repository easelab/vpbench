package se.gu.bg.evaluation

import se.gu.vp.model.{Asset, Feature}

case class RepositoryResults(repo: Asset, compiled: Boolean, numFeatures: Int, scatterDegPerFeature: Seq[Tuple2[Feature, Int]], tangleDegPerFeature: Seq[Tuple2[Feature, Int]])
case class IterationResults(iteration: Int, changed: Boolean, repoResults: Seq[RepositoryResults])
case class AggregatedResults(duration: Long, numberOfCompiledIterations: Int, numberOfAppliedChanges: Int)