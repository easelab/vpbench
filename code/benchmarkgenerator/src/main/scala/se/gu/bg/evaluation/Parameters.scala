package se.gu.bg.evaluation

case class Parameters(numIterations: Int, readInPath: String, insertionPointLimitations: List[String], probDistributionNumber: Int, probDistribution: Array[Double], maxAttempts: Int, discardUselessChangesProb: Double)
