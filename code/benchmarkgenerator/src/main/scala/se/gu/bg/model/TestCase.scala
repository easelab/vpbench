package se.gu.bg.model

import java.nio.file
import java.nio.file.Path

class TestCase(val packageName: String, val className: String, val testName: String) {
  def getClassFilePath : Path = {
    val packagePath = file.Paths.get(packageName.replace('.','/'))
    packagePath.resolve(s"$className.class")
  }

  override def toString: String = s"$packageName.$className.$testName"
}

object TestCase {
  def apply(packageName: String, className: String, testName: String):TestCase = new TestCase(packageName, className, testName)

  def parseLineToTestcase(line: String) : TestCase = {
    val splitLine = line.split("  ")
    TestCase(splitLine(0), splitLine(1), splitLine(2))
  }
}