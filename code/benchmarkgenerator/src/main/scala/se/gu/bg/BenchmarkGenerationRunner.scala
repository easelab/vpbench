package se.gu.bg

import generators.{CloneAssetGenerator, _}
import se.gu.bg.Utilities.almostEqual
import se.gu.vp.model._
import se.gu.vp.operations.{RemoveFeature, _}
import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}

import se.gu.bg.evaluation.{IterationResults, RepositoryResults}
import se.gu.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.bg.operations.AddExternalFeature
import se.gu.vp.operations.Utilities.{extractFeatures, transformASTToList, transformFMToList}

import scala.util.Random
import scala.collection.mutable

class BenchmarkGenerationRunner(val rootAsset: Asset, val operationGenerators: List[Generator], config: Option[Map[Generator,Double]] = None) {

  // setup probability distribution to identify which generator will take action -> might need to be refactored to include parametrization issues
  var probThresholds = config match {
    case None =>
      val uniformProb = 1.0 / operationGenerators.length
      val probs = uniformList(uniformProb, operationGenerators.length)
      val thresholds = probs.scan(0.0)(_ + _)
      val tuples = operationGenerators zip thresholds
      val map :mutable.Map[Generator,Double] = mutable.Map[Generator,Double]()
      for((op,probThresh) <- tuples)
      { map += (op -> probThresh) }
      map
    case Some(map) =>
      // If config is set, all Generators need to be given probabilities
      assert(map.keySet.toList.sortWith(_.getClass.getName < _.getClass.getName) == operationGenerators.sortWith(_.getClass.getName < _.getClass.getName))
      // If config is set, all probabilities summed together need to equal 1
      assert(almostEqual(map.toSeq.map(tuple => tuple._2).sum, 1, 0.001))

      val threshMap :mutable.Map[Generator,Double] = mutable.Map[Generator,Double]()
      var currentThreshold = 0.0
      for(gen <- operationGenerators) {
        threshMap += (gen -> currentThreshold)
        currentThreshold += map(gen)
      }
      threshMap
  }
  // MakeFeatureOptional? AddFeature not existent as Operation

  def uniformList[T](element: T, count:Int) : List[T] = {
    require(count >= 0)
    count match {
      case 0 => Nil
      case _ => element :: uniformList[T](element, count - 1)
    }
  }

  // use traits to uniformly access devOperations? -> cant since its companion objects -> use generators instead

  // TODO: neue Generatoren einbauen und testen + MethodFunktionalität in CloneAssetGenerator testen
  // setup parameter generators for supported operations
  //  var operationGenerators = Map(AddAsset -> AddAssetGenerator(rootAsset), ChangeAsset -> ChangeAssetGenerator(rootAsset), CloneAsset -> CloneAssetGenerator(rootAsset))

  def run(numIterations: Int, targetPath: String, exitCondition: Option[Asset => Boolean] = None, maxAttempts: Int = 50, currentIteration: Int = 0): List[IterationResults] = {
    val tgtPath = file.Paths.get(targetPath)
    exitCondition match {

      // Termination criteria: exitCondition returned true
      case Some(fun)
        if fun(rootAsset) => Nil

      // Termination criteria: Current number of iterations is higher than numIterations
      case _
        if currentIteration > numIterations => Nil

      // Output initial system without changes
      case _
        if currentIteration == 0 =>
          rootAsset.prettyprint

          val currDir = tgtPath.resolve(s"v$currentIteration")
          Files.createDirectories(currDir)
          SerialiseAssetTree(rootAsset, currDir.toAbsolutePath.toString)

          val iterationStatisticsPerRepo = for {
            repository <- transformASTToList(rootAsset).filter(a => a.assetType == RepositoryType)
            relativeRepoPath = file.Paths.get(repository.name.split("\\\\").takeRight(2).mkString("\\"))
            repoPath = currDir.toAbsolutePath.resolve(relativeRepoPath)
          } yield {
            val compiled = executeGradle(repoPath.toString, "compileJava")

            val features = transformFMToList(repository.featureModel.get)
              .filter(feat => feat.getClass != classOf[RootFeature])
              .filterNot(feat => feat.name == "UnAssigned" && feat.parent.get.getClass == classOf[RootFeature])
            val numFeatures = features.length
            val scatteringDegrees = features.map(feat => (feat, feat.mappedAssets(repository).length))
            val tanglingDegrees = features.map(feat => (feat, feat.mappedAssets(repository).flatMap(assets => extractFeatures(assets.presenceCondition)).filter(otherFeat => feat != otherFeat).distinct.length))

            RepositoryResults(repository, compiled, numFeatures, scatteringDegrees, tanglingDegrees)
          }

          val iterationStatisticsTotal = IterationResults(currentIteration, false, iterationStatisticsPerRepo)

          iterationStatisticsTotal :: run(numIterations, targetPath, exitCondition, maxAttempts = maxAttempts, currentIteration = currentIteration + 1)

      // Execute normally
      case _ =>
        val opOption = generateNextOperation(maxAttempts)
        if (opOption.isDefined) {
          opOption.get match {
            case aef: AddExternalFeature =>
              val AddExternalFeature(repository, insertionPoint, project, testCase) = aef
              println("Generated: ")
              println(s"$repository, ${insertionPoint._1}, $project, $testCase")
            case rmf: RemoveFeature =>
            case _ =>
          }
        }
        //rootAsset.prettyprint

        val currDir = tgtPath.resolve(s"v$currentIteration")
        Files.createDirectories(currDir)
        SerialiseAssetTree(rootAsset, currDir.toAbsolutePath.toString)

        val iterationStatisticsPerRepo = for {
          repository <- transformASTToList(rootAsset).filter(a => a.assetType == RepositoryType)
          relativeRepoPath = file.Paths.get(repository.name.split("\\\\").takeRight(2).mkString("\\"))
          repoPath = currDir.toAbsolutePath.resolve(relativeRepoPath)
        } yield {
          val compiled = executeGradle(repoPath.toString, "compileJava")

          val features = transformFMToList(repository.featureModel.get)
            .filter(feat => feat.getClass != classOf[RootFeature])
            .filterNot(feat => feat.name == "UnAssigned" && feat.parent.get.getClass == classOf[RootFeature])
          val numFeatures = features.length
          val scatteringDegrees = features.map(feat => (feat, feat.mappedAssets(repository).length))
          val tanglingDegrees = features.map(feat => (feat, feat.mappedAssets(repository).flatMap(assets => extractFeatures(assets.presenceCondition)).filter(otherFeat => feat != otherFeat).distinct.length))

          RepositoryResults(repository, compiled, numFeatures, scatteringDegrees, tanglingDegrees)
        }

        val iterationStatisticsTotal = IterationResults(currentIteration, opOption.isDefined, iterationStatisticsPerRepo)
        println(s"Change was generated: ${opOption.isDefined}")

        iterationStatisticsTotal :: run(numIterations, targetPath, exitCondition, maxAttempts = maxAttempts, currentIteration = currentIteration + 1)
    }
  }


  def generateNextOperation(maxAttempts: Int) : Option[Operation] = {
    // just works on probDistribution, thus no general implementation
    def argmax(a: Generator, b: Generator) = {
      if (probThresholds.get(a).get >= probThresholds.get(b).get)
      { a }
      else
      { b }
    }

    val random = Random.nextDouble()

    // first: filter for all keys, whose threshold is beneath or equal to random
    // second: select the key with the maximum threshold, that was still passed -> that is the selected operation
    val opGenerator = probThresholds.keySet.
      filter(op => probThresholds.get(op).get <= random).
      reduce( (a,b) => argmax(a,b) )

    // can extract info here for changing probability distributions, if it resulted in None?
    tryToGenerate(opGenerator, maxAttempts)
  }

  def tryToGenerate(generator: Generator, remainingAttempts: Int) : Option[Operation] = {
    generator.generate match {
      case None
        if remainingAttempts <= 0 => None
      case None
        if remainingAttempts > 0 => tryToGenerate(generator, remainingAttempts - 1)
      case Some(operation) => Some(operation)
    }
  }

  // nicht mehr nötig, am Ehesten für ProbDist oder so (wenn Parametrisierung früher eine Rolle spielt)
  def updateGenerators = {
    //    operationGenerators.values.foreach(gen => gen.update)
  }
}