package se.gu.bg.generators

import se.gu.vp.model._
import se.gu.vp.operations.{AssetCloneHelpers, PropagateAsset}
import se.gu.vp.operations.Utilities._

import scala.util.Random

class PropagateAssetGenerator(val rootAsset:Asset) extends Generator{
  override def generate: Option[PropagateAsset] = {
    val filteredTraces = TraceDatabase.traces.map(t => (t.source,t.target)).distinct
    val (selectedSource, selectedTarget) = filteredTraces(Random.nextInt(filteredTraces.length))

    // for debugging
    println(s"PropagateAsset from ${selectedSource} to ${selectedTarget}")

    val generated = new PropagateAsset(source = selectedSource, target = selectedTarget)
    Some(generated)
  }
}

object PropagateAssetGenerator {
  def apply(rootAsset:Asset) : PropagateAssetGenerator = new PropagateAssetGenerator(rootAsset)
}