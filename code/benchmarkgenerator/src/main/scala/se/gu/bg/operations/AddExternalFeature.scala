package se.gu.bg.operations

import se.gu.bg.model.{Project, TestCase}
import se.gu.vp.model.Asset
import se.gu.vp.operations.Operation

case class AddExternalFeature(repository: Asset, insertionPoint: Tuple2[Asset, Int], project: Project, testCase: TestCase) extends Operation {
  // TODO: might need to move AssetTree adaption to perform function? -> currently this is just a storage object -> would require a lot more parameters to function or calculation herein if possible
  override def perform(): Boolean = {
    true
  }
}
