package se.gu.bg.generators

import se.gu.vp.model._
import se.gu.vp.operations.ChangeAsset
import se.gu.vp.operations.Utilities._

import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}

import scala.util.Random

abstract class ChangeAssetGenerator extends Generator {
  val rootAsset: Asset
  val targetPath: Path
  val readInPath: Path

  override def generate: Option[ChangeAsset] = {
    val assets = transformASTToList(rootAsset)
    val changeableAssets = assets
      .filter(a => a.assetType == BlockType)
    val selectedAsset = changeableAssets(Random.nextInt(changeableAssets.length))

    // for debugging
    println(s"ChangeAsset $selectedAsset")

    changeAsset(selectedAsset)
  }

  def changeAsset(asset: Asset) : Option[ChangeAsset]
}