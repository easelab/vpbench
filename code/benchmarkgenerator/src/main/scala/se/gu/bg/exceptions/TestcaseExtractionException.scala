package se.gu.bg.exceptions

case class TestcaseExtractionException(message: String) extends Exception
