package se.gu.bg.operations

import se.gu.vp.model.ASTWellFormednessConstraints.containable
import se.gu.vp.model.{Asset, BlockType}
import se.gu.vp.operations.{AddAsset, Operation}
import se.gu.vp.operations.Utilities.getImmediateAncestorFeatureModel
import se.gu.vp.commons.Logging

import scala.reflect.runtime.universe.MethodType


case class AddAssetAtPosition(override val asset: Asset, override val parent: Asset, insertionPoint: Int) extends AddAsset(asset: Asset, parent: Asset) {

  override def perform(): Boolean = {

    //Checking AST Well-Formedness constraints to disallow additions that invalidate the AST
    if( containable(asset, parent) && insertionPoint <= parent.children.length) {
      //Establishing parent child relationships between asset and target container
      asset.parent = Some(parent)
      val preceding = parent.children.take(insertionPoint)
      val post = parent.children.drop(insertionPoint)
      parent.children = preceding ++ (asset :: post)
      if (asset.getRoot() != None) {
        //Updating global version number, and assigning it to the newly added asset and the parent
        incrementGlobalVersion(asset)
        asset.versionNumber = getGlobalVersion(asset)
        parent.versionNumber = asset.versionNumber
      }
      if(asset.assetType != MethodType & asset.assetType != BlockType){}
      //info(this.getClass.getName + " - Added Asset : " + asset.name + " to : "+parent.name)

      //Traversing through the asset's presence condition to find the mapped features
      //Finding the closest ancestor with a feature model and checking if the feature
      //model contains the features mapped to the assets
      val closestAncestor = getImmediateAncestorFeatureModel( Some(asset) )
      closestAncestor match {
        case Some( fm ) => //Find the features from the presence condition not contained in the feature model
          val missingImplementation = asset.missingFeatures(fm)
          missingImplementation.foreach(
            m => {
              //Add every missing feature in the target feature model's unassigned features
              m.parent = Option(closestAncestor.get.rootfeature.UNASSIGNED)
              closestAncestor.get.rootfeature.UNASSIGNED.subfeatures = m :: closestAncestor.get.rootfeature.UNASSIGNED.subfeatures
            }
          )
        case _ => ;
      }
      true
    } else {
      false
    }
  }
}
