package se.gu.bg

import java.nio.file
import java.nio.file.Path

import se.gu.bg.model.Project

object TestConfigurationProvider {
  def reactiveXDisposableObserverProjTest(rootProject : Project, subprojectName : String) : Tuple3[Project,Path,String] = {
    val subproject = rootProject.getSubprojectByName(subprojectName).get
    val classOrigin = file.Paths.get("build\\classes\\java\\test\\io\\reactivex\\rxjava3\\observers\\DisposableObserverTest.class")
    val path = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin"
    (subproject, classOrigin, path)
  }

  def createReactiveXProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\RxJava")
    val subprojects = Nil
    val name = "rxjava"
    val jar = Some(file.Paths.get("build\\libs\\rxjava-3.0.0-SNAPSHOT.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxPluginsSmaliInputProj() : Project = {
    val pathToGitRepo = file.Paths.get("jadx-smali-input")
    val subprojects = Nil
    val name = "jadx-smali-input"
    val jar = Some(file.Paths.get("build\\libs\\jadx-smali-input-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxPluginsPluginsApiProj() : Project = {
    val pathToGitRepo = file.Paths.get("jadx-plugins-api")
    val subprojects = Nil
    val name = "jadx-plugins-api"
    val jar = Some(file.Paths.get("build\\libs\\jadx-plugins-api-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxPluginsJavaConvertProj() : Project = {
    val pathToGitRepo = file.Paths.get("jadx-java-convert")
    val subprojects = Nil
    val name = "jadx-java-convert"
    val jar = Some(file.Paths.get("build\\libs\\jadx-java-convert-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxPluginsDexInputProj() : Project = {
    val pathToGitRepo = file.Paths.get("jadx-dex-input")
    val subprojects = Nil
    val name = "jadx-dex-input"
    val jar = Some(file.Paths.get("build\\libs\\jadx-dex-input-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxPluginsProj() : Project = {
    val pathToGitRepo = file.Paths.get("jadx-plugins")
    val jadxPluginsDexInputProj = createJadxPluginsDexInputProj()
    val jadxPluginsJavaConvertProj = createJadxPluginsJavaConvertProj()
    val jadxPluginsPluginsApiProj = createJadxPluginsPluginsApiProj()
    val jadxPluginsSmaliInputProj = createJadxPluginsSmaliInputProj()
    val subprojects = Seq(jadxPluginsDexInputProj,jadxPluginsJavaConvertProj, jadxPluginsPluginsApiProj, jadxPluginsSmaliInputProj)
    val name = "jadx-plugins"
    val jar = Some(file.Paths.get("build\\libs\\jadx-plugins-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }


  def createJadxSamplesProj() : Project = {
    val pathToGitRepo = file.Paths.get("jadx-samples")
    val subprojects = Nil
    val name = "jadx-samples"
    val jar = Some(file.Paths.get("build\\libs\\samples-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxGuiProj() : Project = {
    val pathToGitRepo = file.Paths.get("jadx-gui")
    val subprojects = Nil
    val name = "jadx-gui"
    val jar = Some(file.Paths.get("build\\libs\\jadx-gui-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxCoreProj() : Project = {
    val pathToGitRepo = file.Paths.get("jadx-core")
    val subprojects = Nil
    val name = "jadx-core"
    val jar = Some(file.Paths.get("build\\libs\\jadx-core-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }


  def createJadxCliProj() : Project = {
    val pathToGitRepo = file.Paths.get("jadx-cli")
    val subprojects = Nil
    val name = "jadx-cli"
    val jar = Some(file.Paths.get("build\\libs\\jadx-cli-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx")
    val jadxCliProj = createJadxCliProj()
    val jadxCoreProj = createJadxCoreProj()
    val jadxGuiProj = createJadxGuiProj()
    val jadxSamplesProj = createJadxSamplesProj()
    val jadxPluginsProj = createJadxPluginsProj()
    val subprojects = Seq(jadxCliProj,jadxCoreProj,jadxGuiProj,jadxSamplesProj,jadxPluginsProj)
    val name = "jadx"
    val jar = Some(file.Paths.get("build\\libs\\jadx-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  // just for testing
  def jadxProjTest(rootProject : Project, subprojectName : String, classOrigin : String) : Tuple3[Project,Path,String] = {
    val subproject = rootProject.getSubprojectByName(subprojectName).get
    val classOriginPath = file.Paths.get(classOrigin)
    val path = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin"
    (subproject,classOriginPath,path)
  }

  def jadxCliRenameConverterProjTest(rootProject : Project, subprojectName : String) : Tuple3[Project,Path,String] =
    {
      val subproject = rootProject.getSubprojectByName(subprojectName).get
      val classOrigin = file.Paths.get("build\\classes\\java\\test\\jadx\\cli\\RenameConverterTest.class")
      val path = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin"
      (subproject,classOrigin,path)
    }

  def structurizrClientEncryptedJsonTests(rootProject : Project, subprojectName : String) : Tuple3[Project,Path,String] =
    {
      val subproject = rootProject.getSubprojectByName(subprojectName).get
      val classOrigin = file.Paths.get("build\\classes\\java\\test\\com\\structurizr\\io\\json\\EncryptedJsonTests.class")
      val path = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin"
      (subproject,classOrigin,path)
    }

  def createStructurizrExamplesProj() : Project = {
    val path = file.Paths.get("structurizr-examples")
    val subprojects = Nil
    val name = "structurizr-examples"
    val jar = Some(file.Paths.get("build/libs/structurizr-examples-1.8.1.jar"))
    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("test\\unit")
    new Project(path, subprojects, name, jar, srcPath, testPath)
  }

  def createStructurizrCoreProj() : Project = {
    val path = file.Paths.get("structurizr-core")
    val subprojects = Nil
    val name = "structurizr-core"
    val jar = Some(file.Paths.get("build/libs/structurizr-core-1.8.1.jar"))
    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("test\\unit")
    new Project(path, subprojects, name, jar, srcPath, testPath)
  }

  def createStructurizrClientProj() : Project = {
    val path = file.Paths.get("structurizr-client")
    val subprojects = Nil
    val name = "structurizr-client"
    val jar = Some(file.Paths.get("build/libs/structurizr-client-1.8.1.jar"))
    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("test\\unit")
    new Project(path, subprojects, name, jar, srcPath, testPath)
  }

  def createStructurizrProj() : Project =
    {
      val pathToGitRepo = file.Paths.get("D:\\Dokumente\\_Studium\\masterarbeit\\gitClones\\Structurizr\\java")
      val structurizrClientProj = createStructurizrClientProj()
      val structurizrCoreProj = createStructurizrCoreProj()
      val structurizrExamplesProj = createStructurizrExamplesProj()
      val name = "structurizr"
      val jar = None
      val srcPath = file.Paths.get("src")
      val testPath = file.Paths.get("test\\unit")

      val project = new Project(pathToGitRepo, Seq(structurizrClientProj,structurizrCoreProj,structurizrExamplesProj), name, jar, srcPath, testPath)
      project.subprojects.foreach(sub => sub.parent = Some(project))
      project
    }

  def disruptorUtilProjTest(rootProject : Project, subprojectName : String) : Tuple3[Project,Path,String] = {
    val subproject = rootProject.getSubprojectByName(subprojectName).get
    val classOrigin = file.Paths.get("build\\classes\\java\\test\\com\\lmax\\disruptor\\util\\UtilTest.class")
    val path = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin"
    (subproject,classOrigin,path)
  }

  def createJsonJavaProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\JSON-java")
    val subprojects = Nil
    val name = "JSON-java"
    val jar = Some(file.Paths.get("build/libs/JSON-java-v20200429-SNAPSHOT.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createDisruptorProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\_Studium\\masterarbeit\\gitClones\\Disruptor\\disruptor")
    val subprojects = Nil
    val name = "disruptor"
    val jar = Some(file.Paths.get("build/libs/disruptor-4.0.0-SNAPSHOT.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createAlgorithmsProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Algorithms")
    val subprojects = Nil
    val name = "Algorithms"
    val jar = Some(file.Paths.get("build/libs/Algorithms.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createTeammatesProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\teammates")
    val subprojects = Nil
    val name = "teammates"
    val jar = Some(file.Paths.get("build/libs/teammates.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createTeammatesProj2() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\teammates")
    val subprojects = Nil
    val name = "teammates"
    val jar = Some(file.Paths.get("build/libs/teammates.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\e2e\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createTeammatesProj3() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\teammates")
    val subprojects = Nil
    val name = "teammates"
    val jar = Some(file.Paths.get("build/libs/teammates.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\lnp\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createTeammatesProj4() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\teammates")
    val subprojects = Nil
    val name = "teammates"
    val jar = Some(file.Paths.get("build/libs/teammates.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createNLPProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\CoreNLP")
    val subprojects = Nil
    val name = "CoreNLP"
    val jar = Some(file.Paths.get("build/libs/CoreNLP-4.2.0.jar"))

    val srcPath = file.Paths.get("src\\")
    val testPath = file.Paths.get("test\\src\\")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createEBProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\EventBus\\EventBus")
    val subprojects = Nil
    val name = "eventbus"
    val jar = Some(file.Paths.get("build/libs/eventbus-3.2.0.jar"))

    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createEBAnnotationProcessorProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\EventBus\\EventBusAnnotationProcessor")
    val subprojects = Nil
    val name = "eventbus-annotation-processor"
    val jar = Some(file.Paths.get("build/libs/eventbus-annotation-processor-3.2.0.jar"))

    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createEBPerformanceProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\EventBus\\EventBusPerformance")
    val subprojects = Nil
    val name = "EventBusPerformance"
    val jar = None

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createEBTestProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\EventBus\\EventBusTest")
    val subprojects = Nil
    val name = "EventBusTest"
    val jar = None

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createEBTestJavaProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\EventBus\\EventBusTestJava")
    val subprojects = Nil
    val name = "EventBusTestJava"
    val jar = Some(file.Paths.get("build/libs/EventBusTestJava.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\main\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createEBTestSubscriberInJarProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\EventBus\\EventBusTestSubscriberInJar")
    val subprojects = Nil
    val name = "EventBusTestSubscriberInJar"
    val jar = Some(file.Paths.get("build/libs/EventBusTestSubscriberInJar-3.0.0.jar"))

    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("src\\test\\java")

    new Project(pathToGitRepo, subprojects, name, jar, srcPath, testPath)
  }

  def createEventbusProj() : Project = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\EventBus")
    val ebProj = createEBProj()
    val ebAnnProcProj = createEBAnnotationProcessorProj()
    val ebPerformanceProj = createEBPerformanceProj()
    val ebTestProj = createEBTestProj()
    val ebTestJavaProj = createEBTestJavaProj()
    val ebTestSubInJarProj = createEBTestSubscriberInJarProj()
    val name = "EventBus"
    val jar = None
    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("test\\unit")

    val project = new Project(pathToGitRepo, Seq(ebProj, ebAnnProcProj, ebPerformanceProj, ebTestProj, ebTestJavaProj, ebTestSubInJarProj), name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def structurizr() : Tuple5[String,Path,Map[Path,String],Map[String,Path],String] = {
    val classpath = "\"D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java\\structurizr-core\\build\\libs\\structurizr-core-1.8.1.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java\\structurizr-client\\build\\libs\\structurizr-client-1.8.1.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java\\structurizr-examples\\build\\libs\\structurizr-examples-1.8.1.jar\""
    val rootProjectPath = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java")

    // TODO: Test with files from rootProjectDir (-> path = ""?) like ->
    // D:\Dokumente\Entwicklung\Eclipse_Workspace_J11\TestBase_structurizr_compile_Component_addCrosscuttingSubproj_three_jadxjdeps --class-path "structurizr\structurizr-client\build\libs\structurizr-client-1.8.1.jar;structurizr\structurizr-core\build\libs\structurizr-core-1.8.1.jar;build\libs\TestBase_structurizr_compile_Component_addCrosscuttingSubproj_three_jadx.jar" -recursive build\classes\java\main\com\hello\HelloWorld.class
    // Assume 1-to-1 mapping between directories and subprojectnames //TODO: does not work for mainproject (different name due to settings.gradle)
    val projectDirToJarMap = Map(
      file.Paths.get("structurizr-core") -> "structurizr-core-1.8.1.jar",
      file.Paths.get("structurizr-client") -> "structurizr-client-1.8.1.jar",
      file.Paths.get("structurizr-examples") -> "structurizr-examples-1.8.1.jar"
    )
    ////    val projectDirToJarMap = Map(
    ////      "structurizr-core" -> "structurizr-core-1.8.1.jar",
    ////      "structurizr-client" -> "structurizr-client-1.8.1.jar",
    ////      "structurizr-examples" -> "structurizr-examples-1.8.1.jar"
    ////    )
    // For case where we do not assume unique names for .jars -> needs change in getDependenciesRecursivelyWIndepClasspaths (-> stores .jar, too)
    //    val projectDirToJarMap = Map(
    //      "D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java\\structurizr-core" -> "D:/Dokumente/Entwicklung/Eclipse_Workspace_J11/GitClones/Structurizr/java/structurizr-core/build/libs/structurizr-core-1.8.1.jar",
    //      "D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java\\structurizr-client" -> "D:/Dokumente/Entwicklung/Eclipse_Workspace_J11/GitClones/Structurizr/java/structurizr-client/build/libs/structurizr-client-1.8.1.jar",
    //      "D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java\\structurizr-examples" -> "D:/Dokumente/Entwicklung/Eclipse_Workspace_J11/GitClones/Structurizr/java/structurizr-examples/build/libs/structurizr-examples-1.8.1.jar"
    //    )

    val jarToProjectDirMap = projectDirToJarMap.map(_.swap)
    val path = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin"
    (classpath,rootProjectPath,projectDirToJarMap,jarToProjectDirMap,path)
  }

  def structurizrClient() : Tuple9[String,String,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath,rootProjectPath,projectDirToJarMap,jarToProjectDirMap,path) = structurizr()
    //TODO: move classPathClass to structurizr()
    val classPathClass = "\"D:/Dokumente/Entwicklung/Eclipse_Workspace_J11/GitClones/Structurizr/java/structurizr-core/build/classes/java/main;D:/Dokumente/Entwicklung/Eclipse_Workspace_J11/GitClones/Structurizr/java/structurizr-client/build/classes/java/main\""
    val originProjectPath = file.Paths.get("structurizr-client")
    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("test\\unit")
    (classpath,classPathClass,rootProjectPath,originProjectPath,srcPath,testPath,projectDirToJarMap,jarToProjectDirMap,path)
  }


  def structurizrCore() : Tuple9[String,String,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath,rootProjectPath,projectDirToJarMap,jarToProjectDirMap,path) = structurizr()
    //TODO: move classPathClass to structurizr()
    val classPathClass = "\"D:/Dokumente/Entwicklung/Eclipse_Workspace_J11/GitClones/Structurizr/java/structurizr-core/build/classes/java/main\""
    val originProjectPath = file.Paths.get("structurizr-core")
    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("test\\unit")
    (classpath,classPathClass,rootProjectPath,originProjectPath,srcPath,testPath,projectDirToJarMap,jarToProjectDirMap,path)
  }

  // Structurizr-Core -> ComponentTest-case
  def structurizrCoreComponentTest() : Tuple10[String,String,Path,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath, classPathClass, rootProjectPath, originProjectPath, srcPath, testPath, projectDirToJarMap, jarToProjectDirMap, path) = structurizrCore()
    val classOrigin = file.Paths.get("build\\classes\\java\\test\\com\\structurizr\\model\\ComponentTests.class")
    (classpath,classPathClass,rootProjectPath,originProjectPath,classOrigin,srcPath,testPath, projectDirToJarMap, jarToProjectDirMap, path)
  }

  // Structurizr-Core -> StringUtils-case
  def structurizrCoreStringUtilsTest() : Tuple10[String,String,Path,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath, classPathClass, rootProjectPath, originProjectPath, srcPath, testPath, projectDirToJarMap, jarToProjectDirMap, path) = structurizrCore()
    val classOrigin = file.Paths.get("build\\classes\\java\\test\\com\\structurizr\\util\\StringUtilsTests.class")
    (classpath,classPathClass,rootProjectPath,originProjectPath,classOrigin,srcPath,testPath, projectDirToJarMap, jarToProjectDirMap, path)
  }

  // Structurizr-Client -> EncryptedJsonTest-case
  def structurizrClientStringUtilsTest() : Tuple10[String,String,Path,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath, classPathClass, rootProjectPath, originProjectPath, srcPath, testPath, projectDirToJarMap, jarToProjectDirMap, path) = structurizrClient()
    val classOrigin = file.Paths.get("build\\classes\\java\\test\\com\\structurizr\\io\\json\\EncryptedJsonTests.class")
    (classpath,classPathClass,rootProjectPath,originProjectPath,classOrigin,srcPath,testPath, projectDirToJarMap, jarToProjectDirMap, path)
  }

  def reactiveX() : Tuple8[String,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val classpath = "\"D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\RxJava\\build\\libs\\rxjava-3.0.0-SNAPSHOT.jar\""
    val rootProjectPath = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\RxJava")
    val originProjectPath = file.Paths.get("")
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val projectDirToJarMap = Map(
      file.Paths.get("") -> "rxjava-3.0.0-SNAPSHOT.jar"
    )
    val jarToProjectDirMap = projectDirToJarMap.map(_.swap)
    val path = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin"
    (classpath,rootProjectPath,originProjectPath,srcPath,testPath,projectDirToJarMap,jarToProjectDirMap,path)
  }

  def reactiveXSchedulerTest() : Tuple9[String,Path,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath, rootProjectPath, originProjectPath, srcPath, testPath, projectDirToJarMap, jarToProjectDirMap, path) = reactiveX()
    val classOrigin = file.Paths.get("build\\classes\\java\\test\\io\\reactivex\\rxjava3\\core\\SchedulerTest.class")
    (classpath,rootProjectPath,originProjectPath,classOrigin,srcPath,testPath, projectDirToJarMap, jarToProjectDirMap, path)
  }

  def reactiveXDisposableObserverTest() : Tuple9[String,Path,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath, rootProjectPath, originProjectPath, srcPath, testPath, projectDirToJarMap, jarToProjectDirMap, path) = reactiveX()
    val classOrigin = file.Paths.get("build\\classes\\java\\test\\io\\reactivex\\rxjava3\\observers\\DisposableObserverTest.class")
    (classpath,rootProjectPath,originProjectPath,classOrigin,srcPath,testPath, projectDirToJarMap, jarToProjectDirMap, path)
  }

  def jadx() : Tuple5[String,Path,Map[Path,String],Map[String,Path],String] = {
    val classpath = "\"D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\build\\libs\\jadx-dev.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\jadx-cli\\build\\libs\\jadx-cli-dev.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\jadx-core\\build\\libs\\jadx-core-dev.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\jadx-gui\\build\\libs\\jadx-gui-dev.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\jadx-samples\\build\\libs\\samples-dev.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\jadx-plugins\\build\\libs\\jadx-plugins-dev.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\jadx-plugins\\jadx-dex-input\\build\\libs\\jadx-dex-input-dev.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\jadx-plugins\\jadx-java-convert\\build\\libs\\jadx-java-convert-dev.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\jadx-plugins\\jadx-plugins-api\\build\\libs\\jadx-plugins-api-dev.jar;D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx\\jadx-plugins\\jadx-smali-input\\build\\libs\\jadx-small-input-dev.jar\""
    val rootProjectPath = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\jadx")

    val projectDirToJarMap = Map(
      file.Paths.get("") -> "jadx-dev.jar",
      file.Paths.get("jadx-cli") -> "jadx-cli-dev.jar",
      file.Paths.get("jadx-core") -> "jadx-core-dev.jar",
      file.Paths.get("jadx-gui") -> "jadx-gui-dev.jar",
      file.Paths.get("jadx-samples") -> "samples-dev.jar",
      file.Paths.get("jadx-plugins") -> "jadx-plugins-dev.jar",
      file.Paths.get("jadx-plugins\\jadx-dex-input") -> "jadx-dex-input-dev.jar",
      file.Paths.get("jadx-plugins\\jadx-java-convert") -> "jadx-java-convert-dev.jar",
      file.Paths.get("jadx-plugins\\jadx-plugins-api") -> "jadx-plugins-api-dev.jar",
      file.Paths.get("jadx-plugins\\jadx-smali-input") -> "jadx-smali-input-dev.jar"
    )
    val jarToProjectDirMap = projectDirToJarMap.map(_.swap)
    val path = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin"
    (classpath,rootProjectPath,projectDirToJarMap,jarToProjectDirMap,path)
  }

  def jadxCli() : Tuple8[String,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath,rootProjectPath,projectDirToJarMap,jarToProjectDirMap,path) = jadx()
    val originProjectPath = file.Paths.get("jadx-cli")
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")
    (classpath,rootProjectPath,originProjectPath,srcPath,testPath,projectDirToJarMap,jarToProjectDirMap,path)
  }

  def jadxCliRenameConverterTest() : Tuple9[String,Path,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath, rootProjectPath, originProjectPath, srcPath, testPath, projectDirToJarMap, jarToProjectDirMap, path) = jadxCli()
    val classOrigin = file.Paths.get("build\\classes\\java\\test\\jadx\\cli\\RenameConverterTest.class")
    (classpath,rootProjectPath,originProjectPath,classOrigin,srcPath,testPath, projectDirToJarMap, jarToProjectDirMap, path)
  }

  def disruptor() : Tuple8[String,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val classpath = "\"D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\disruptor\\build\\libs\\disruptor-4.0.0-SNAPSHOT.jar\""
    val rootProjectPath = file.Paths.get("D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\disruptor")
    val originProjectPath = file.Paths.get("")
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val projectDirToJarMap = Map(
      file.Paths.get("") -> "disruptor-4.0.0-SNAPSHOT.jar"
    )
    val jarToProjectDirMap = projectDirToJarMap.map(_.swap)
    val path = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin"
    (classpath,rootProjectPath,originProjectPath,srcPath,testPath,projectDirToJarMap,jarToProjectDirMap,path)
  }

  def disruptorUtilTest() : Tuple9[String,Path,Path,Path,Path,Path,Map[Path,String],Map[String,Path],String] = {
    val (classpath, rootProjectPath, originProjectPath, srcPath, testPath, projectDirToJarMap, jarToProjectDirMap, path) = disruptor()
    val classOrigin = file.Paths.get("build\\classes\\java\\test\\com\\lmax\\disruptor\\util\\UtilTest.class")
    (classpath,rootProjectPath,originProjectPath,classOrigin,srcPath,testPath, projectDirToJarMap, jarToProjectDirMap, path)
  }

  // TODO: add assumption somewhere sensible: dependent testclasses only in same (sub?)project
  //TODO: How to get rootDir of multirepo? Only via rootDir and projectPath?
}
