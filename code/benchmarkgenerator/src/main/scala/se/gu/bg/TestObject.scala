package se.gu.bg

import java.io.File
import java.nio.file

import se.gu.bg.Utilities.{getAllIndicesOf, replaceStringSlice}

import scala.util.Random

object TestObject {

  def test(list: List[Int]) : Int = {
    if (list.length == 0) {
      0
    } else {
      Random.nextInt(list.length)
    }
  }

  def main(args: Array[String]) : Unit = {

    val currentPath = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir\\CalculatorRoot\\BasicCalculator\\src\\main\\java\\org\\easelab\\calculatorexample\\basiccalculator"
    val test = new File(file.Paths.get(System.getProperty("user.dir")).resolve(currentPath).normalize.toString)

    val content = List("This", "is", "a", "superb", "list", "!", "return None;", "}")

    val indexedContent : IndexedSeq[(Int,String)] = for {
      index <- 0 until content.length
      line = content(index)
    } yield {
      (index,line)
    }

    val test1 = indexedContent.sortBy(tuple => tuple._1)
    val test2 = indexedContent.filter(tuple => tuple._1 != 2)
      .sorted

    val a = 1

//    val testList1 = List(1,2,3,4,5)
//    val testList2 = List()
//    val testList3 = List(1)
//
//    println(test(testList1))
//    println(test(testList2))
//    println(test(testList3))

//    val str1 = "Lorem ipsum."
//    val str2 = "Its a me."
//
//    println(replaceStringSlice(str1,str2,2,4))
//    println(replaceStringSlice(str1,str2,3,3))
//    println(replaceStringSlice(str1,str2,2,25))
//
//    val str3 = "TestHeyHeyTest"
//
//    println(str3.replace("Test","Temp"))
//    println(getAllIndicesOf(str3,"Hey"))
  }
}