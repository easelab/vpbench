package se.gu.bg.generators

import com.rits.cloning.Cloner
import se.gu.vp.model.{Asset, AssetType, Feature, FeatureModel, Path, RootFeature}
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath, transformASTToList, transformFMToList}
import se.gu.vp.operations.{Operation, RemoveFeature, SerialiseAssetTree}
import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}
import java.util
import java.util.Comparator

import se.gu.bg.Utilities.{cleanDirectory, findRepoOnUpPath}
import se.gu.bg.gradlehandler.GradleExecutor.executeGradle

import scala.util.Random

class RemoveFeatureGenerator (val rootAsset: Asset, targetPathString: String, readInPathString: String) extends Generator{
  val targetPath = file.Paths.get(targetPathString)
  val readInPath = file.Paths.get(readInPathString)

  def removeFeature(featureToRemove: Feature, containingAsset: Asset, originalFeatureList: List[Tuple2[Feature,Asset]]): Option[RemoveFeature] = {
    val cloner = new Cloner
    cloner.dontCloneInstanceOf(classOf[AssetType])
    cloner.registerConstant(None)
    val assetTreeCopy = cloner.deepClone(rootAsset)

    val originalAssetList = transformASTToList(rootAsset)
    val cloneAssetList = transformASTToList(assetTreeCopy)
    val originalToCloneAssetMap = new util.IdentityHashMap[Asset, Asset]()
    originalAssetList.zip(cloneAssetList).foreach(tuple => originalToCloneAssetMap.put(tuple._1, tuple._2))

    val cloneFeatureList = getAllFeaturesInAsset(assetTreeCopy)
      .map(elem => elem._1)
    val originalToCloneFeatureMap = originalFeatureList
      .map(elem => elem._1)
      .zip(cloneFeatureList).toMap

    val cloneAssetPath = computeAssetPath(originalToCloneAssetMap.get(containingAsset))
    val cloneFeaturePath = computeFeaturePath(originalToCloneFeatureMap(featureToRemove))
    val clonePath = se.gu.vp.model.Path(assetTreeCopy, cloneAssetPath ++ cloneFeaturePath)

    RemoveFeature(clonePath)

    cleanDirectory(targetPath, false)
    SerialiseAssetTree(assetTreeCopy, targetPath.toString)

    val affectedRepository = findRepoOnUpPath(containingAsset).get
    val affectedRepoOldPath = file.Paths.get(affectedRepository.name)
    val affectedRepoRelPath = affectedRepoOldPath.subpath(readInPath.getNameCount - 1, affectedRepoOldPath.getNameCount)
    val affectedRepoPath = targetPath.resolve(affectedRepoRelPath)

    val compiled = executeGradle(affectedRepoPath.toString, "compileJava")
    println(compiled)

    if (compiled) {
      val originalAssetPath = computeAssetPath(containingAsset)
      val originalFeaturePath = computeFeaturePath(featureToRemove)
      val originalPath = se.gu.vp.model.Path(rootAsset, originalAssetPath ++ originalFeaturePath)
      println(s"Removed feature: ${featureToRemove.name}")
      Some(RemoveFeature(originalPath))
    } else {
      None
    }
  }

  override def generate: Option[RemoveFeature] = {
    val features = getAllFeaturesInAsset(rootAsset)
    val removableFeatures = features
      .filter(feat => feat._1.getClass != classOf[RootFeature])
      .filter(feat => feat._1.name != "UnAssigned")
    if (removableFeatures.length == 0) {
      None
    } else {
      val (featureToRemove, containingAsset) = removableFeatures(Random.nextInt(removableFeatures.length))
      removeFeature(featureToRemove, containingAsset, features)
    }
  }

  def getAllFeaturesInAsset(asset: Asset) : List[Tuple2[Feature, Asset]] = {
    val featureModels = transformASTToList(asset).map(elem => (elem.featureModel,elem))
    val filteredFeatureModels = featureModels
      .filter(elem => elem._1.isDefined)
      .map(tuple => (tuple._1.get,tuple._2))
    val featuresWithAsset = filteredFeatureModels.flatMap(tuple => transformFMToList(tuple._1).map(feat => (feat, tuple._2)))
    featuresWithAsset
    //featuresWithAsset.map(elem => elem._1.map(feature => (feature,elem._2))).flatten
  }

//  def getFeatureModelsInAsset(asset: Asset) : List[Tuple2[Option[FeatureModel], Asset]] = {
//    val featureModel = asset.featureModel
//    val childrenFMs = asset.children
//      .flatMap(child => getFeatureModelsInAsset(child))
//    (featureModel, asset) :: childrenFMs
//  }
}

object RemoveFeatureGenerator{
  def apply(rootAsset: Asset, targetPathString: String, readInPathString: String):RemoveFeatureGenerator = new RemoveFeatureGenerator(rootAsset, targetPathString, readInPathString)
}