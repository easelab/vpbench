package se.gu.bg.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations._

// newNames since apply for multiple assets exists in vp
class ChangeAssetName( assetToChange:Option[Asset] = None , assetsToChange:Option[List[Asset]] = None , newName:Option[String] , newNames:Option[List[String]]) extends ChangeAsset with Logging {
  perform()
  override def perform(): Boolean = {
    if(assetToChange != None && newName != None) {
      assetToChange.get.name = newName.get

      incrementGlobalVersion(assetToChange.get)
      assetToChange.get.versionNumber = getGlobalVersion(assetToChange.get)
      //info(this.getClass.getName + " - Asset changed : " + assetToChange.get.name)
      true
    }

    if(assetsToChange != None && newNames != None && assetsToChange.get.length == newNames.get.length)
    {

      var versionIncremented = false;

      for((asset,name) <- assetsToChange.get.zip(newNames.get)){
        if(!versionIncremented){
          incrementGlobalVersion( asset )
          versionIncremented=true;
        }
        asset.name = name
        asset.versionNumber = getGlobalVersion(asset)
        //info(this.getClass.getName + " - Asset changed : " + asset.name)
      }
    }
    true
  }
}
object ChangeAssetName{
  def apply(assetToChange: Asset, newName: String): ChangeAssetName = new ChangeAssetName(Option(assetToChange), None, Option(newName), None)

  def apply(assetsToChange: List[Asset], newNames: List[String]): ChangeAssetName = new ChangeAssetName(None, Option(assetsToChange), None, Option(newNames))
}