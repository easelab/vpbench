package se.gu.bg

import java.nio.file

import se.gu.bg.TestConfigurationProvider.{createDisruptorProj, createJadxProj, createReactiveXProj, createStructurizrProj}
import se.gu.bg.generators.{AddFeatureGenerator, RemoveFeatureGenerator}
import se.gu.vp.model.TraceDatabase
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.readInAssetTree
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.SerialiseAssetTree
import se.gu.vp.operations.Utilities.findAssetByName

object TestSerializeAfterTransplant {
  def main(args: Array[String]): Unit = {
    val readInPath = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot"
    //val readInPath = "D:\\Dokumente\\_Studium\\masterarbeit\\test\\2021-04-19_09-00\\v40\\CalculatorRoot"

    val ast = readInJavaAssetTree(readInPath).get
    val parentAsset1 = findAssetByName("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\BasicCalculator\\src\\main\\java\\org\\easelab\\calculatorexample\\basiccalculator\\BasicCalculator.java", ast).get.children(1).children(2)
    val parentAsset2 = findAssetByName("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\BasicCalculator\\src\\main\\java\\org\\easelab\\calculatorexample\\basiccalculator\\Main.java", ast).get.children(1).children(0)

    val structurizrProject = createStructurizrProj()
    val reactiveXProject = createReactiveXProj()
    val jadxProject = createJadxProj()
    val disruptorProject = createDisruptorProj()
    val rootProjects = List(structurizrProject, disruptorProject)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPathString = readInPath)
    val removeFeatureGenerator = RemoveFeatureGenerator(ast, "..\\testEnvironment\\TestTransplant\\", readInPath)

    val structurizrCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val structurizrClient = structurizrProject.getSubprojectByName("structurizr-client").get
    //contains enum issue in EncryptionLocation.
    val encryptedJsonTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.io.json" && tc.className == "EncryptedJsonTests" && tc.testName == "test_write_and_read").get
    val decryptNotTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.encryption" && tc.className == "AesEncryptionStrategyTests" && tc.testName == "test_decrypt_doesNotDecryptTheCiphertext_WhenTheIncorrectKeySizeIsUsed").get
    // problem, that Model.java is not entirely written out (-> file is very long stopped after 27477 characters, file length is 50142 characters -> check if everything is part of assetTree?)
    val anotherTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.io.json" && tc.className == "EncryptedJsonWriterTests" && tc.testName == "test_write_ThrowsAnIllegalArgumentException_WhenANullWriterIsSpecified").get
    // reverted Vertex problem
    val vertexTest = structurizrCore.testcases.find(tc => tc.packageName == "com.structurizr.view" && tc.className == "VertexTests" && tc.testName == "test_equals").get
    val apiStructurizrClientTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.api" && tc.className == "StructurizrClientTests" && tc.testName == "test_construction_WithTwoParameters").get
    val apiResponseTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.api" && tc.className == "ApiResponseTests" && tc.testName == "test_parse_createsAnApiErrorObjectWithTheSpecifiedErrorMessage").get
    val viewSetTest = structurizrCore.testcases.find(tc => tc.packageName == "com.structurizr.view" && tc.className == "ViewSetTests" && tc.testName == "test_createContainerView_ThrowsAnException_WhenADuplicateKeyIsSpecified").get
    val disruptorTest = disruptorProject.testcases.find(tc => tc.packageName == "com.lmax.disruptor" && tc.className == "FixedSequenceGroupTest" && tc.testName == "shouldReturnMinimumOf2Sequences").get

    //addFeatureGenerator.extractTestcase((parentAsset2,0),structurizrClient, decryptNotTest)
    //addFeatureGenerator.extractTestcase()
    addFeatureGenerator.extractTestcase((parentAsset2,3),structurizrClient, encryptedJsonTest)
    addFeatureGenerator.extractTestcase((parentAsset1,0),disruptorProject, disruptorTest)
    addFeatureGenerator.extractTestcase((parentAsset2,1),structurizrClient, anotherTest)
    val listOfTraces = TraceDatabase.traces
    //SerialiseAssetTree(ast, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TestTransplant_Original")
    //addFeatureGenerator.extractTestcase((parentAsset2,0),structurizrClient, anotherTest)
    //addFeatureGenerator.extractTestcase((parentAsset2,0),structurizrClient, decryptNotTest)

    //addFeatureGenerator.extractTestcase((parentAsset2,0),structurizrClient, vertexTest)
    //addFeatureGenerator.extractTestcase((parentAsset2,0),structurizrClient, apiStructurizrClientTest)
    //addFeatureGenerator.extractTestcase((parentAsset2,0),structurizrClient, apiResponseTest)
    //addFeatureGenerator.extractTestcase((parentAsset2,0),structurizrClient, viewSetTest)

    SerialiseAssetTree(ast, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TestTransplant_Original")
  }
}
