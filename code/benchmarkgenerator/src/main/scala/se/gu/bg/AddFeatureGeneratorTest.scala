package se.gu.bg

import se.gu.bg.TestConfigurationProvider.{createDisruptorProj, createJadxProj, createReactiveXProj, createStructurizrProj, structurizr}
import se.gu.bg.generators.{AddFeatureGenerator, RemoveFeatureGenerator}
import se.gu.vp.model.{Asset, AssetPathElement, AssetType, FileType, MethodType, Path, RepositoryType, TraceDatabase, VPRootType}
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.readInAssetTree
import se.gu.vp.operations.Utilities.{findAssetByName, resolveAssetPath}
import java.nio.file

object AddFeatureGeneratorTest {
//  def main(args: Array[String]): Unit = {
//    val readInPath = "D:\\Dokumente\\_Studium\\masterarbeit\\test\\test\\CalculatorRoot"
//
//    val ast = readInAssetTree(readInPath).get
//    val parentAsset1 = findAssetByName("D:\\Dokumente\\_Studium\\masterarbeit\\test\\test\\CalculatorRoot\\AdvancedCalculator\\AdvancedCalculator.js", ast).get.children(1)
//    val parentAsset2 = findAssetByName("D:\\Dokumente\\_Studium\\masterarbeit\\test\\test\\CalculatorRoot\\BasicCalculator\\BasicCalculator.js", ast).get.children(1)
//
//    val structurizrProject = createStructurizrProj()
//    val reactiveXProject = createReactiveXProj()
//    val jadxProject = createJadxProj()
//    val disruptorProject = createDisruptorProj()
//    val rootProjects = List(structurizrProject, reactiveXProject, jadxProject, disruptorProject)
//
//    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir", readInPath)
//    val removeFeatureGenerator = RemoveFeatureGenerator(ast, "..\\testEnvironment\\TestTransplant\\", readInPath)
//
//    val structurizrClient = structurizrProject.getSubprojectByName("structurizr-client").get
//    val encryptedJsonTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.io.json" && tc.className == "EncryptedJsonTests" && tc.testName == "test_write_and_read").get
//    val apiStructurizrClientTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.api" && tc.className == "StructurizrClientTests" && tc.testName == "test_construction_WithTwoParameters").get
//    val apiResponseTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.api" && tc.className == "ApiResponseTests" && tc.testName == "test_parse_createsAnApiErrorObjectWithTheSpecifiedErrorMessage").get
//
//    val tracesInit = TraceDatabase.traces
//    //addFeatureGenerator.extractTestcase((parentAsset1,0),structurizrClient, encryptedJsonTest)
//    val tracesIntermediate = TraceDatabase.traces
//    //removeFeatureGenerator.generate
//    addFeatureGenerator.extractTestcase((parentAsset2,0),structurizrClient, encryptedJsonTest)
//    val tracesEnd = TraceDatabase.traces
//    //addFeatureGenerator.extractTestcase((parentAsset2,0),structurizrClient, encryptedJsonTest)
//    val tracesEndEnd = TraceDatabase.traces
//    val test = 1
//  }
}