package se.gu.bg.generators

import se.gu.bg.Utilities.{resolveFeatureFromPath, resolveFeatureModelFromPath}
import se.gu.vp.model._
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator
import se.gu.vp.operations.CloneFeature
import se.gu.vp.operations.Utilities._

import scala.util.Random

// reproduce NullExceptionError
class ReproduceCloneFeatureGenerator(val rootAsset:Asset) extends Generator {
  val basicCalc = getassetbyname("D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\vp4\\vp\\CalculatorRoot\\BasicCalculator",rootAsset).get
  val basicCalcRF = basicCalc.featureModel.get.rootfeature
  val advCalc = getassetbyname("D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\vp4\\vp\\CalculatorRoot\\AdvancedCalculator",rootAsset).get
  val advCalcRF = advCalc.featureModel.get.rootfeature
  val exprSolv = getassetbyname("D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\vp4\\vp\\CalculatorRoot\\ExpressionSolver",rootAsset).get
  val exprSolvRF = exprSolv.featureModel.get.rootfeature

  val basicAdd = getFeatureByNameFromFeature("BasicCalculator-Add",basicCalcRF).get
  val postfix = getFeatureByNameFromFeature("ExpressionSolver-Solver-Postfix",exprSolvRF).get
  val modulus = getFeatureByNameFromFeature("AdvancedCalculator-Modulus",advCalcRF).get

  val basicAddP = Path(rootAsset, computeAssetPath(basicCalc) ++ computeFeaturePath(basicAdd))
  val postfixP = Path(rootAsset, computeAssetPath(exprSolv) ++ computeFeaturePath(postfix))
  val modulusP = Path(rootAsset, computeAssetPath(advCalc) ++ computeFeaturePath(modulus))

  val it = Iterator((basicAddP,postfixP),(basicAddP,modulusP))

  def generateCandidates: Set[Asset] = {
    Nil.toSet
  }

  override def generate: Option[CloneFeature] = {
    if (it.hasNext)
    {
      val (source, target) = it.next()
      return Some(CloneFeature(source,target))
    }

    val divide = getFeatureByNameFromFeature("BasicCalculator-Divide",basicCalcRF).get
    val divideP = Path(rootAsset, computeAssetPath(basicCalc) ++ computeFeaturePath(divide))
    val exprAdd = getFeatureByNameFromFeature("BasicCalculator-Add",exprSolvRF).get
    val add2P = Path(rootAsset, computeAssetPath(exprSolv) ++ computeFeaturePath(exprAdd))

    return Some(CloneFeature(divideP,add2P))

    val assets = transformASTToList(rootAsset)

    // calculate asset/feature-paths
    // contains RootFeatures and UnAssigned -> check if this makes sense or needs to be removed (at least UnAssigned?)
    // selecting a RootFeature here still results in cloning UnAssigned-"feature"
    // TODO: could be optimized to contain only clonable features, i.e. features with existing targets
    val featurePaths = for {
      asset <- assets
      if asset.featureModel.isDefined
      asPath = computeAssetPath(asset)
      feature <- transformFMToList(asset.featureModel.get)
      // removes "UnAssigned"-features from list of possible clonable features
      if !(feature.name == "UnAssigned" && feature.parent.isDefined && feature.parent.get == feature.getFeatureModelRoot().get)
      fePath = computeFeaturePath(feature)
    } yield {
      Path(rootAsset, asPath ++ fePath)
    }

    // should only be the case, if no feature models is exist or no features to be cloned
    if (featurePaths.length == 0)
      return None

    // generate source feature
    val sourcefp = featurePaths(Random.nextInt(featurePaths.length))
    val sourceFeature = resolveFeatureFromPath(sourcefp).get
    // filter out "UnAssigned" as existence of "UnAssigned"-feature in other feature model counts as duplicate (only occurs when trying to clone a rootFeature, as only those contain an "UnAssigned"-feature)
    val sourceFeatureTree = (sourceFeature :: flattenSubFeatures(sourceFeature.subfeatures)).filter(f => f.name != "UnAssigned")

    // filter set of possible target feature paths by making sure, that neither the sourcefeature or any of its subfeatures exists in target paths feature model
    // TODO: can be optimized by filtering by feature models, as every fm needs only to be checked once
    val filterFeaturePaths =
    for {
      fp <- featurePaths
      fm = resolveFeatureModelFromPath(fp).get
      targetfeatures = transformFMToList(fm)
      if sourceFeatureTree.forall(sf => !targetfeatures.contains(sf))
    } yield fp

    if (filterFeaturePaths.length == 0)
      return None

    //generated target feature
    val targetfp = filterFeaturePaths(Random.nextInt(filterFeaturePaths.length))

    // for debugging
    println(s"Clone $sourcefp into $targetfp")

    val generated = new CloneFeature(sourceFeature = sourcefp, targetParentFeature = targetfp)
    Some(generated)
  }
}

object ReproduceCloneFeatureGenerator{
  def apply(rootAsset: Asset):ReproduceCloneFeatureGenerator = new ReproduceCloneFeatureGenerator(rootAsset)
}