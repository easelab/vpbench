package se.gu.bg.generators
import java.nio.file
import java.nio.file.{Files, Path}
import java.util
import java.util.Comparator

import com.rits.cloning.Cloner
import se.gu.bg.Utilities.{cleanDirectory, findFolderParentAsset, findRepoOnUpPath}
import se.gu.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.bg.operations.ChangeAssetContent
import se.gu.vp.model.{Asset, AssetType, BlockType}
import se.gu.vp.operations.Utilities.transformASTToList
import se.gu.vp.operations.{ChangeAsset, SerialiseAssetTree}

import scala.util.Random

class AddLineGenerator(override val rootAsset: Asset, targetPathString: String, readInPathString: String, uselessChangeDiscardProb: Double) extends ChangeAssetGenerator{
  override val targetPath: Path = file.Paths.get(targetPathString)
  override val readInPath: Path = file.Paths.get(readInPathString)

  // identifies empty lines and single line comments
  val lineUsefulnessRegex = "\\s*(//.*)?".r

  override def changeAsset(asset: Asset): Option[ChangeAsset] = {
    val cloner = new Cloner
    cloner.dontCloneInstanceOf(classOf[AssetType])
    cloner.registerConstant(None)
    val assetTreeCopy = cloner.deepClone(rootAsset)

    val originalAssetList = transformASTToList(rootAsset)
    val cloneAssetList = transformASTToList(assetTreeCopy)
    val originalToCloneAssetMap = new util.IdentityHashMap[Asset, Asset]()
    originalAssetList.zip(cloneAssetList).foreach(tuple => originalToCloneAssetMap.put(tuple._1, tuple._2))

    val cloneSelectedAsset = originalToCloneAssetMap.get(asset)

    if (asset.content == None) {
      None
    } else {
      val content = asset.content.get

      val indexedContent = for {
        index <- 0 until content.length
        line = content(index)
      } yield {
        (index,line)
      }

      // addLineIndex is the linenumber, where the new line will be, + 1 allows it to be inserted at the end
      val addLineIndex = Random.nextInt(content.length + 1)
      // select new line from any block inside the same folder (package) -> intermediate point between FileType and RepositoryType -> tradeoff between applicability (due to visibility) and amount of possibilities
      val potentialStatements = transformASTToList(findFolderParentAsset(asset).get)
        .filter(elem => elem.assetType == BlockType)
        .flatMap(block => block.content)
        .flatten

      val addLine = potentialStatements(Random.nextInt(potentialStatements.length))
      if (addLine.matches(lineUsefulnessRegex.regex)) {
        if (Random.nextDouble <= uselessChangeDiscardProb) {
          return None
        }
      }


      val newPreContent = indexedContent
        .filter(elem => elem._1 < addLineIndex)
        .sortBy(tuple => tuple._1)
        .toList
        .map(elem => elem._2)
      val newPostContent = indexedContent
        .filter(elem => elem._1 >= addLineIndex)
        .sortBy(tuple => tuple._1)
        .toList
        .map(elem => elem._2)

      val newContent = newPreContent ++ (addLine :: newPostContent)

      ChangeAssetContent(cloneSelectedAsset, newContent)

      cleanDirectory(targetPath, false)
      SerialiseAssetTree(assetTreeCopy, targetPath.toString)

      val affectedRepository = findRepoOnUpPath(asset).get
      val affectedRepoOldPath = file.Paths.get(affectedRepository.name)
      val affectedRepoRelPath = affectedRepoOldPath.subpath(readInPath.getNameCount - 1, affectedRepoOldPath.getNameCount)
      val affectedRepoPath = targetPath.resolve(affectedRepoRelPath)

      val compiled = executeGradle(affectedRepoPath.toString, "compileJava")
      println(compiled)

      if (compiled) {
        println(s"Added line: $addLine")
        Some(ChangeAssetContent(asset, newContent))
      } else {
        None
      }
    }
  }
}
object AddLineGenerator {
  def apply(rootAsset:Asset, targetPathString: String, readInPathString: String, uselessChangeDiscardProb: Double = 0.0) : AddLineGenerator = new AddLineGenerator(rootAsset, targetPathString, readInPathString, uselessChangeDiscardProb)
}
