﻿using System.Configuration;
using CommandLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABB.Swum;
using ABB.Swum.Nodes;
using Antlr3.ST;
using CommandLine.Text;
using WM.UnitTestScribe.CallGraph;
using WM.UnitTestScribe.Summary;
using WM.UnitTestScribe.TestCaseDetector;
using ABB.SrcML.Data;
using TeaCap.TestPropagator;
using TeaCap.GitMining;
using System.Threading;
using System.Globalization;
using System.Xml.Linq;
using ABB.SrcML;

namespace WM.UnitTestScribe {
    public class Program {
        //Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
        
        /// <summary> Subject application location </summary>
        //public static readonly string LocalProj = @"D:\Research\myTestSubjects\Callgraph\CallgraphSubject";
        //public static readonly string LocalProj = @"D:\Research\Subjects\google-api-dotnet-client-master";
        //public static readonly string LocalProj = @"D:\Research\Subjects\Sando-master";
        public static readonly string LocalProj = ConfigurationManager.AppSettings["sourceProjectFolder"];
        public static readonly string targetProject = ConfigurationManager.AppSettings["targetProjectFolder"];
        
        //public static readonly string LocalProj = @"D:\Research\Subjects\Glimpse-master";
        /// <summary> SrcML directory location </summary>
        public static readonly string SrcmlLoc = ConfigurationManager.AppSettings["srcMLexeFolder"];
        public static readonly string outputLoc = ConfigurationManager.AppSettings["srcMLCallGraphOutputFolder"];

        public static readonly string testcaseOutput = "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases\\testcases.txt";
        public static readonly string testcaseExtract = "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases";


        /// <summary>
        /// Command line testing
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args) {
            DateTime dt = DateTime.Now;
            //args = new string[] { "hello" };
            //args = new string[] { "testcases", "--loc", LocalProj, "--srcmlPath", SrcmlLoc }; 
            //args = new string[] { "fid_testcase_save", "--loc", LocalProj, "--srcmlPath", SrcmlLoc, "--outputLoc", testcaseOutput };

            //args = new string[] { "extractTestcase", "--loc", LocalProj, "--srcmlPath", SrcmlLoc, "--outputLoc", testcaseExtract, "--namespace", "com.structurizr.api", "--class", "HashBasedMessageAuthenticationCodeTests", "--test", "test_generate" };
            //args = new string[] { "extractTestcase", "--loc", LocalProj, "--srcmlPath", SrcmlLoc, "--outputLoc", testcaseExtract, "--namespace", "com.structurizr.api", "--class", "StructurizrClientTests", "--test", "test_getWorkspace_ThrowsAnException_WhenTheWorkspaceIdIsNotValid" };
            //args = new string[] { "extractTestcase", "--loc", LocalProj, "--srcmlPath", SrcmlLoc, "--outputLoc", testcaseExtract, "--namespace", "com.structurizr.encryption", "--class", "AesEncryptionStrategyTests", "--test", "test_encrypt_EncryptsPlaintext" };
            //args = new string[] { "extractTestcase", "--loc", LocalProj, "--srcmlPath", SrcmlLoc, "--outputLoc", testcaseExtract, "--namespace", "com.structurizr.io.json", "--class", "EncryptedJsonTests", "--test", "test_write_and_read" };
            //args = new string[] { "extractTestcase", "--loc", "D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java\\structurizr-client\\test\\unit", "--srcmlPath", SrcmlLoc, "--outputLoc", "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases\\structurizr\\structurizr-client\\com\\structurizr\\io\\json\\EncryptedJsonTests\\test_write_and_read", "--namespace", "com.structurizr.io.json", "--class", "EncryptedJsonTests", "--test", "test_write_and_read" };
            //args = new string[] { "extractTestcase", "--loc", "D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java\\structurizr-core\\test\\unit", "--srcmlPath", SrcmlLoc, "--outputLoc", "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases\\structurizr\\structurizr-core\\com\\structurizr\\view\\VertexTests\\test_equals", "--namespace", "com.structurizr.view", "--class", "VertexTests", "--test", "test_equals" };
            //args = new string[] { "extractTestcase", "--loc", "D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java\\structurizr-core\\test\\unit", "--srcmlPath", SrcmlLoc, "--outputLoc", "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases\\structurizr\\structurizr-core\\com\\structurizr\\view\\ViewSetTests\\test_createContainerView_ThrowsAnException_WhenADuplicateKeyIsSpecified", "--namespace", "com.structurizr.view", "--class", "ViewSetTests", "--test", "test_createContainerView_ThrowsAnException_WhenADuplicateKeyIsSpecified" };

            //args = new string[] { "summary", "--loc", LocalProj, "--srcmlPath", SrcmlLoc, "--outputLoc",  outputLoc }; 
            var options = new Options();
            string invokedVerb = null;
            object invokedVerbOptions = null;


            if (!CommandLine.Parser.Default.ParseArguments(args, options,
                (verb, verbOptions) => {
                    invokedVerb = verb;
                    invokedVerbOptions = verbOptions;
                })) {
                Environment.Exit(CommandLine.Parser.DefaultExitCodeFail);
            }
            if (invokedVerb == "callgraph") {
                var callGraphOp = (CallgraphOptions)invokedVerbOptions;
                var generator = new InvokeCallGraphGenerator(callGraphOp.LocationsPath, callGraphOp.SrcMLPath);
                generator.run();
            } else if (invokedVerb == "testcases") {
                var testCaseOp = (TestCaseDetectOptions)invokedVerbOptions;
                var detector = new TestCaseDetector.TestCaseDetector(testCaseOp.LocationsPath, testCaseOp.SrcMLPath);
                detector.AnalyzeTestCases();
                Console.WriteLine("print testcases");
                foreach (var testCaseId in detector.AllTestCases) {
                    Console.WriteLine(testCaseId.NamespaceName + "  "+ testCaseId.ClassName + "  " + testCaseId.MethodName);
                }
            }
            else if (invokedVerb == "fid_testcase_save")
            {
                var testCaseOp = (SaveIdTestCaseDetectOptions)invokedVerbOptions;
                var detector = new TestCaseDetector.TestCaseDetector(testCaseOp.LocationsPath, testCaseOp.SrcMLPath);
                detector.AnalyzeTestCases();
                Console.WriteLine("print testcases");
                var identifiedTests = detector.AllTestCases.Where(tc => tc.NamespaceName != String.Empty && tc.ClassName != String.Empty && tc.MethodName != String.Empty);
                var idTestStrings = identifiedTests
                    .Select(idt => idt.NamespaceName + "  " + idt.ClassName + "  " + idt.MethodName);
                
                File.WriteAllLines(testCaseOp.OutputLoc, idTestStrings);
            }
            else if (invokedVerb == "extractTestcase")
            {
                var extractionOp = (TestcaseExtractOptions)invokedVerbOptions;
                var fullTestPath = extractionOp.Namespace + "." + extractionOp.ClassName + "." + extractionOp.TestName;
                
                if (!Directory.Exists(extractionOp.OutputLoc))
                {
                    Directory.CreateDirectory(extractionOp.OutputLoc);
                }

                var fileName = Path.Combine(extractionOp.LocationsPath, extractionOp.Namespace.Replace('.', '\\'), extractionOp.ClassName + ".java");
                var xmlOutput = Path.Combine(extractionOp.OutputLoc, "tmpxmloutput.xml");
                var xmlFilterOutput = Path.Combine(extractionOp.OutputLoc, "tmpxmloutput2.xml");
                var src2srcmlexe = Path.Combine(extractionOp.SrcMLPath, "src2srcml.exe");
                var srcml2srcexe = Path.Combine(extractionOp.SrcMLPath, "srcml2src.exe");

                Src2XML sx = new Src2XML();
                XML2Src xs = new XML2Src();
                sx.SourceFileToXml(fileName, xmlOutput, src2srcmlexe);
                Thread.Sleep(2000);
                xs.XmlToSourceFile(xmlOutput, xmlFilterOutput, srcml2srcexe, String.Format("--xpath \"/src:unit//src:function[src:name='{0}']/src:block\"", extractionOp.TestName));
                Thread.Sleep(2000);
                XElement elementRoot = XElement.Load(xmlFilterOutput, LoadOptions.PreserveWhitespace);

                var methodBlock = elementRoot.DescendantsAndSelf().Where(e => e.Name == SRC.Block).First();
                var methodClosure = methodBlock.ToSource();
                var methodContent = methodClosure.Substring(1,methodClosure.Length - 2);

                using (var project = new DataProject<CompleteWorkingSet>(extractionOp.LocationsPath, extractionOp.LocationsPath, extractionOp.SrcMLPath))
                {
                    project.Update();
                    NamespaceDefinition globalNamespace;
                    project.WorkingSet.TryObtainReadLock(5000, out globalNamespace);
                    
                    var methods = globalNamespace.GetDescendants<MethodDefinition>();
                    
                    // To get some evidence for my understanding about Expression Statements and other Statements
                    var assertion = Util.GetChildStatementsRecursively(globalNamespace)
                        .Where(st => st.ChildStatements.Count != 0 && st.Content != null)
                        .Any();
                    
                    if (assertion)
                    {
                        throw new Exception("ChildStatement and Content are set in one statement.");
                    }
                    
                    var testToExtract = methods.Where(mtd => mtd.GetFullName() == fullTestPath).Single();
                    var testFile = testToExtract.PrimaryLocation.SourceFileName;
                    
                    var ancestorStatements = testToExtract.GetAncestorsAndSelf();
                    
                    // TODO: Check if there can be other types, that can represent imports?
                    var importStatements = ancestorStatements
                        .First(elem => elem is NamespaceDefinition)
                        .GetDescendants<ImportStatement>()
                        .Where(st => st.PrimaryLocation.SourceFileName == testFile);
                    var aliasStatements = ancestorStatements
                        .First(elem => elem is NamespaceDefinition)
                        .GetDescendants<AliasStatement>()
                        .Where(st => st.PrimaryLocation.SourceFileName == testFile);
                    
                    var importStatementResult = importStatements.OfType<Statement>().Concat(aliasStatements);
                    var importStringList = importStatementResult
                        .Select(st => st.PrimaryLocation)
                        .Select(st => (st.SourceFileName, st.StartingLineNumber, st.StartingColumnNumber, st.EndingLineNumber, st.EndingColumnNumber))
                        .Select(st => Util.ExtractStatementFromFile(st.SourceFileName, st.StartingLineNumber, st.StartingColumnNumber, st.EndingLineNumber, st.EndingColumnNumber))
                        .ToList();
                    
                    var importString = String.Join("", importStringList);
                    
                    //var methodStatements = testToExtract.ChildStatements;
                    
                    //var expressions = methodStatements.Select(
                    //    mtdst => Util.GetChildStatementsRecursively(mtdst)
                    //        .Select(st => st.Content)
                    //        .Where(ex => ex != null)
                    //        .Where(ex => ex.ToString().ContainsCaseInsensitive("assert"))
                    //        .Select(ex => ex.ParentStatement));
                    
                    //var methodStringList = methodStatements
                    //    .Select(st => st.PrimaryLocation)
                    //    .Select(st => (st.SourceFileName, st.StartingLineNumber, st.StartingColumnNumber, st.EndingLineNumber, st.EndingColumnNumber))
                    //    .Select(st => Util.ExtractStatementFromFile(st.SourceFileName, st.StartingLineNumber, st.StartingColumnNumber, st.EndingLineNumber, st.EndingColumnNumber))
                    //    .ToList();
                    
                    //var methodString = String.Join("", methodStringList);

                    // Write extractions to file
                    //if (!Directory.Exists(extractionOp.OutputLoc))
                    //{
                    //    Directory.CreateDirectory(extractionOp.OutputLoc);
                    //}

                    var importExtractFile = Path.Combine(extractionOp.OutputLoc, "imports.java");
                    var testcaseExtractFile = Path.Combine(extractionOp.OutputLoc, "testcase.java");

                    File.WriteAllText(importExtractFile, importString);
                    //File.WriteAllText(testcaseExtractFile, methodString);
                    File.WriteAllText(testcaseExtractFile, methodContent);
                }
                return;
            }
            else if (invokedVerb == "summary") {
                var SummaryOp = (SummarizeTestOptions)invokedVerbOptions;
                //ProjectCloner projectCloner = new ProjectCloner(SrcmlLoc);
                //projectCloner.cloneRepos();

                DataAnalyzer dataAnalyzer = new DataAnalyzer();
                dataAnalyzer.CountEcoSystems();
                //TestPropagator testPropagator = new TestPropagator(LocalProj, targetProject, SrcmlLoc);
                //testPropagator.propagate();
                //var summary = new SummaryGenerator(SummaryOp.LocationsPath, SummaryOp.SrcMLPath);
                //Console.WriteLine("This is summary");
                //summary.AnalyzeSummary();
                //summary.GenerateSummary(SummaryOp.OutputLoc);
                //Console.WriteLine("Done!!!!!!  Thanks.");

            } else if (invokedVerb == "hello") {

                Console.WriteLine("Hello");
                
                //string proPath = @"C:\Users\boyang.li@us.abb.com\Documents\RunningTest\Input\ConsoleApplication1";
                //string proPath = @"C:\Users\boyang.li@us.abb.com\Documents\RunningTest\Input\SrcML\ABB.SrcML";
                using (var project = new DataProject<CompleteWorkingSet>(LocalProj, LocalProj, SrcmlLoc))
                {
                    project.Update();
                    NamespaceDefinition globalNamespace;
                    project.WorkingSet.TryObtainReadLock(5000, out globalNamespace);
                    try
                    {

                        // Step 1.   Build the call graph
                        Console.WriteLine("{0,10:N0} files", project.Data.GetFiles().Count());
                        Console.WriteLine("{0,10:N0} namespaces", globalNamespace.GetDescendants<NamespaceDefinition>().Count());
                        Console.WriteLine("{0,10:N0} types", globalNamespace.GetDescendants<TypeDefinition>().Count());
                        Console.WriteLine("{0,10:N0} methods", globalNamespace.GetDescendants<MethodDefinition>().Count());
                        Console.ReadLine();
                        var methods = globalNamespace.GetDescendants<MethodDefinition>();

                        // Step 2.   Testing
                        Console.WriteLine("======  test 1 ========= ");
                        foreach (MethodDefinition m in methods)
                        {
                            Console.WriteLine("Method Name : {0}", m.GetFullName());

                        }
                       
                    }
                    finally
                    {
                        project.WorkingSet.ReleaseReadLock();
                    }


                }
              

                Console.ReadLine();
                Console.WriteLine("print hello");

            }
            TimeSpan ts = DateTime.Now - dt;
            Console.WriteLine(ts.ToString());
            Console.ReadLine();
        }

        // TODO: Figure out how options work in C#, especially with these Verbs and InvokedVerbs
        private class Options {
            [VerbOption("callgraph", HelpText = "Analyze stereotype of methods in the project")]
            public CallgraphOptions StereotypeVerb { get; set; }

            [VerbOption("testcases", HelpText = "find all test cases in a project")]
            public TestCaseDetectOptions FindAllTestCaseVerb { get; set; }

            [VerbOption("fid_testcase_save", HelpText = "find and save all test cases in a project")]
            public SaveIdTestCaseDetectOptions SaveAllIdTestCaseVerb { get; set; }

            [VerbOption("extractTestcase", HelpText = "extract a given testcase")]
            public TestcaseExtractOptions ExtractTestCaseVerb { get; set; }

            [VerbOption("summary", HelpText = "summarize test cases in a project")]
            public SummarizeTestOptions SummaryTestCaseVerb { get; set; }

            
            [VerbOption("hello", HelpText = "Print hello for testing")]
            public HelloOptions HelloVerb { get; set; }

            [HelpVerbOption]
            public string GetUsage(string verb) {
                return HelpText.AutoBuild(this, verb);
            }
        }




        /// <summary>
        /// Stereotype detector
        /// </summary>
        private class CallgraphOptions {
            /// <summary> Subject application location </summary>
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }
        }



        /// <summary>
        /// Options for findAllTestCases
        /// </summary>
        private class TestCaseDetectOptions {
            /// <summary> Subject application location </summary>
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }
        }


        /// <summary>
        /// Options for findAndSaveAllIdTestCases
        /// </summary>
        private class SaveIdTestCaseDetectOptions
        {
            /// <summary> Subject application location </summary>
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }

            [Option("outputLoc", Required = true, HelpText = "The path to the .txt-file, where to store the identified testcases")]
            public string OutputLoc { get; set; }
        }

        /// <summary>
        /// Options for extractTestCase
        /// </summary>
        private class TestcaseExtractOptions
        {
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }

            [Option("outputLoc", Required = true, HelpText = "The path, where to store the extracted testcase")]
            public string OutputLoc { get; set; }

            [Option("namespace", Required = true)]
            public string Namespace { get; set; }

            // ClassName due to Location fields, which can get the filename
            [Option("class", Required = true)]
            public string ClassName { get; set; }

            [Option("test", Required = true)]
            public string TestName { get; set; }
        }

        /// <summary>
        /// Stereotype detector
        /// </summary>
private class SummarizeTestOptions {
            /// <summary> Subject application location </summary>
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }



            [Option("outputLoc", Required = true, HelpText = "Summary Output location")]
            public string OutputLoc { get; set; }
        }
      


        /// <summary>
        /// print hello for testing 
        /// </summary>
        private class HelloOptions {
        }
    }

}
