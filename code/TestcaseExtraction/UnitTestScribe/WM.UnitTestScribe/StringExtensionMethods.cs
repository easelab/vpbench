﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WM.UnitTestScribe
{
    public static class StringExtensionMethods
    {
        public static bool ContainsCaseInsensitive(this string str, string contained)
        {
            return str.ToLower().Contains(contained.ToLower());
        }
    }
}