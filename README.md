# A Benchmark-Generator for Evolving Variant-Rich Software #

Materials for the research paper: A Benchmark-Generator for Evolving Variant-Rich Software - Christoph Derks, Daniel Str�ber, Thorsten Berger

### What is contained? ###

* Implementation - code: 
	* Benchmark Generation Framework - benchmarkgenerator
	* UnitTestScribe - TestcaseExtraction/UnitTestScribe
		* cloned from https://github.com/evoresearch/UnitTestScribe and extended from there
		* see UnitTestScribe.txt in TestcaseExtraction for information about forked version
* Evaluation Data - evaluation_data:
	* Six generated version histories + statistics (check params.json in each folder for parametrization)
* Donor systems for transplantation - donor_repos
	* Disruptor
		* cloned from https://github.com/LMAX-Exchange/disruptor
		* see Setup_for_experiments.txt for concrete version information and applied preprocessing steps for this version
			
	* Structurizr
		* cloned from https://github.com/structurizr/java
		* see Setup_for_experiments.txt for concrete version information and applied preprocessing steps for this version

* Initial Systems - initial_systems
	* Calculator
	* JSON-java
		* cloned from https://github.com/stleary/JSON-java
		* see Setup_for_experiments.txt for concrete version information and already applied preprocessing steps

### Regarding params.json###
The ordering of corresponding generators for probability distribution is: 

1. AddLineGenerator
2. ReplaceLineGenerator
3. DeleteLineGenerator
4. AddFeatureGenerator
5. RemoveFeatureGenerator

### How do I get set up? ###

TODO: Will be added at a later point.